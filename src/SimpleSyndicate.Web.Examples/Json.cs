﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System.Collections.Generic;

namespace SimpleSyndicate.Web.Examples
{
    // example of using JsonClient for unauthenticated services
    public static class Json
    {
        // gets a url into a dynamic object
        public static void Get(string url)
        {
            var client = JsonClientFactory.Create();
            var json = client.Get(url);
            var message = json.Message;
        }

        // gets a url into a typed object
        public static JsonItem GetContents(string url)
        {
            var client = JsonClientFactory.Create();
            return client.Get<JsonItem>(url);
        }

        // can also POST and PUT
    }

    // example of using JsonClient for unauthenticated services to retrieve paged results
    public static class JsonWithPaging
    {
        // gets a set of paged records into one object
        public static JsonPage GetPages(string url)
        {
            var client = JsonClientFactory.Create();

            // JsonPage represents a single page of results, with each result being a JsonItem
            var results = client.GetPages<JsonPage, JsonItem>(
                url,                    // url should be for the first page of the results
                (page) => page.Next,    // expression to get the url for the next page
                (page) => page.Items);  // expression to the collection that holds each record

            // whilst JsonPage represents a single page of JsonItems, the items from the other pages are just
            // added to this so you get back a JsonPage that holds the JsonItems from every page
            return results;
        }

        // gets a set of paged records into one object, performing an operation on each item
        public static JsonPage GetPagesManipulatingEachItem(string url)
        {
            var client = JsonClientFactory.Create();

            // JsonPage represents a single page of results, with each result being a JsonItem
            var results = client.GetPages<JsonPage, JsonItem>(
                url,                    // url should be for the first page of the results
                (page) => page.Next,    // expression to get the url for the next page
                (page) => page.Items,   // expression to the collection that holds each record
                (item) => { item.Message = item.Message + "; status is " + item.Status; }); // operation to perform on each item

            // whilst JsonPage represents a single page of JsonItems, the items from the other pages are just
            // added to this so you get back a JsonPage that holds the JsonItems from every page
            return results;
        }
    }

    // example of using JsonClient for authenticated services
    public static class JsonWithAuthentication
    {
        // gets a url into a dynamic object
        public static void Get(string url)
        {
            var client = JsonClientFactory.CreateUsingOAuth2ClientCredentials("endpoint", "clientId", "clientSecret", "scope");
            var json = client.Get(url);
            var message = json.Message;
        }

        // gets a url into a typed object
        public static JsonItem GetContents(string url)
        {
            var client = JsonClientFactory.CreateUsingOAuth2ClientCredentials("endpoint", "clientId", "clientSecret", "scope");
            return client.Get<JsonItem>(url);
        }
    }

    // example item
    public class JsonItem
    {
        public string Message { get; set; }

        public string Status { get; set; }
    }

    // example collection
    public class JsonPage
    {
        public IList<JsonItem> Items { get; set; }

        public string Next { get; set; }
    }
}
