﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System.Net.Http;

namespace SimpleSyndicate.Web.Examples
{
    // example of using RestClient
    public static class Rest
    {
        // gets a url, allowing the response to be manipulated
        public static void Get(string url)
        {
            var client = RestClientFactory.Create();
            var response = client.Get(url);
            var statusCode = response.StatusCode;
            response.Dispose();
        }

        // gets the contents of a url
        public static string GetContents(string url)
        {
            var client = RestClientFactory.Create();
            return client.GetContent(url);
        }

        // post to a url, allowing the response to be manipulated
        public static void Post(string url)
        {
            var client = RestClientFactory.Create();
            using (var content = new StringContent("content"))
            {
                using (var response = client.Post(url, content))
                {
                    var statusCode = response.StatusCode;
                }
            }
        }

        // post to a url, returning the contents
        public static string PostContents(string url)
        {
            var client = RestClientFactory.Create();
            using (var content = new StringContent("content"))
            {
                return client.PostContent(url, content);
            }
        }
    }
}
