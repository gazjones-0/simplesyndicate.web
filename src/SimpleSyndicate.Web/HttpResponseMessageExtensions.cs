﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System.Net.Http;
using System.Threading.Tasks;

namespace SimpleSyndicate.Web
{
    /// <summary>
    /// The <see cref="HttpResponseMessageExtensions"/> class contains methods that extend the <see cref="HttpResponseMessage"/> class.
    /// </summary>
    public static class HttpResponseMessageExtensions
    {
        /// <summary>
        /// Synchronously returns all the content of the response as a string.
        /// </summary>
        /// <param name="message">The <see cref="HttpResponseMessage"/> instance that this method extends.</param>
        /// <returns>The content of the response.</returns>
        public static string GetContent(this HttpResponseMessage message)
        {
            var content = GetContentAsync(message);
            content.Wait();
            return content.Result;
        }

        /// <summary>
        /// Asynchronously returns all the content of the response as a string.
        /// </summary>
        /// <param name="message">The <see cref="HttpResponseMessage"/> instance that this method extends.</param>
        /// <returns>The content of the response.</returns>
        public static async Task<string> GetContentAsync(this HttpResponseMessage message)
        {
            return await message.Content.ReadAsStringAsync().ConfigureAwait(false);
        }
    }
}
