﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace SimpleSyndicate.Web
{
    /// <summary>
    /// Base REST client used for interacting with REST resources, providing the base functionality for interacting with REST resources using JSON.
    /// </summary>
    public abstract partial class RestClientBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RestClientBase"/> class using the specified <paramref name="responseHandler"/> and <paramref name="httpClientFactory"/>.
        /// </summary>
        /// <param name="responseHandler">An <see cref="IRestResponseHandler"/> that is used to check if a REST request was successful or not.</param>
        /// <param name="httpClientFactory">An <see cref="IHttpClientFactory"/> that is used to create HTTP clients that are used to make REST requests.</param>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="responseHandler"/> or <paramref name="httpClientFactory"/> is <c>null</c>.</exception>
        protected RestClientBase(IRestResponseHandler responseHandler, IHttpClientFactory httpClientFactory)
        {
            ResponseHandler = responseHandler ?? throw new ArgumentNullException(nameof(responseHandler));
            HttpClientFactory = httpClientFactory ?? throw new ArgumentNullException(nameof(httpClientFactory));
        }

        /// <summary>
        /// REST operation being performed.
        /// </summary>
        protected enum RestOperation
        {
            /// <summary>
            /// GET operation.
            /// </summary>
            Get,

            /// <summary>
            /// POST operation.
            /// </summary>
            Post,

            /// <summary>
            /// PUT operation.
            /// </summary>
            Put
        }

        /// <summary>
        /// Content type being used for the REST operation.
        /// </summary>
        protected enum RestOperationContentType
        {
            /// <summary>
            /// JSON value.
            /// </summary>
            Json,

            /// <summary>
            /// <see cref="HttpContent"/> value.
            /// </summary>
            HttpContent
        }

        /// <summary>
        /// Gets or sets the response handler used to check if the REST response was successful or not.
        /// </summary>
        /// <value>An <see cref="IRestResponseHandler"/>.</value>
        public IRestResponseHandler ResponseHandler { get; set; }

        /// <summary>
        /// Gets or sets the HTTP client factory used to create HTTP clients that are used to make REST requests.
        /// </summary>
        /// <value>An <see cref="IHttpClientFactory"/>.</value>
        public IHttpClientFactory HttpClientFactory { get; set; }

        /// <summary>
        /// Performs a REST operation using the specified <see cref="HttpClient"/>.
        /// </summary>
        /// <param name="httpClient"><see cref="HttpClient"/> to perform the operation with.</param>
        /// <param name="uri">Uri to the resource.</param>
        /// <param name="operation">Operation to perform.</param>
        /// <param name="operationContentType">Content type being used for the operation.</param>
        /// <param name="value">Optional value for the operation.</param>
        /// <returns>The response.</returns>
        protected static async Task<HttpResponseMessage> PerformRestOperationAsync(HttpClient httpClient, Uri uri, RestOperation operation, RestOperationContentType operationContentType, object value)
        {
            switch (operation)
            {
                case RestOperation.Get:
                    return await httpClient.GetAsync(uri).ConfigureAwait(false);

                case RestOperation.Post:
                    switch (operationContentType)
                    {
                        case RestOperationContentType.HttpContent:
                            return await httpClient.PostAsync(uri.ToString(), (HttpContent)value).ConfigureAwait(false);

                        case RestOperationContentType.Json:
                            return await httpClient.PostAsJsonAsync(uri.ToString(), value).ConfigureAwait(false);
                    }

                    break;

                case RestOperation.Put:
                    switch (operationContentType)
                    {
                        case RestOperationContentType.HttpContent:
                            return await httpClient.PutAsync(uri.ToString(), (HttpContent)value).ConfigureAwait(false);

                        case RestOperationContentType.Json:
                            return await httpClient.PutAsJsonAsync(uri.ToString(), value).ConfigureAwait(false);
                    }

                    break;
            }

            throw new InvalidOperationException("Invalid combination of REST operation and REST content type");
        }

        /// <summary>
        /// Performs a REST operation, returning the content of the response as a string.
        /// </summary>
        /// <param name="uri">Uri to the resource.</param>
        /// <param name="operation">Operation to perform.</param>
        /// <param name="operationContentType">Content type being used for the operation.</param>
        /// <param name="value">Optional value for the operation.</param>
        /// <returns>The content of the response as a string.</returns>
        protected async Task<string> PerformRestContentOperationAsync(Uri uri, RestOperation operation, RestOperationContentType operationContentType, object value)
        {
            using (var response = await PerformRestOperationAsync(uri, operation, operationContentType, value).ConfigureAwait(false))
            {
                return await response.GetContentAsync().ConfigureAwait(false);
            }
        }

        /// <summary>
        /// Performs a REST operation.
        /// </summary>
        /// <param name="uri">Uri to the resource.</param>
        /// <param name="operation">Operation to perform.</param>
        /// <param name="operationContentType">Content type being used for the operation.</param>
        /// <param name="value">Optional value for the operation.</param>
        /// <returns>The response.</returns>
        protected async Task<HttpResponseMessage> PerformRestOperationAsync(Uri uri, RestOperation operation, RestOperationContentType operationContentType, object value)
        {
            // if we've no authentication information, simulate an unauthorised response so the response handler will handle any
            // authentication and give us a new factory; if it doesn't handle authentication, we'll just use the existing factory
            if (HttpClientFactory.AuthenticationHeaderValue == null)
            {
                using (var response = new HttpResponseMessage(System.Net.HttpStatusCode.Unauthorized))
                {
                    var newHttpClientFactory = ResponseHandler.NewHttpClientFactoryToRetryRequestWith(response, HttpClientFactory);
                    if (newHttpClientFactory != null)
                    {
                        HttpClientFactory = newHttpClientFactory;
                    }
                }
            }

            // perform operation
            using (var httpClient = HttpClientFactory.Create())
            {
                var response = await PerformRestOperationAsync(httpClient, uri, operation, operationContentType, value).ConfigureAwait(false);
                var newHttpClientFactoryToRetryRequestWith = ResponseHandler.NewHttpClientFactoryToRetryRequestWith(response, HttpClientFactory);
                if (newHttpClientFactoryToRetryRequestWith != null)
                {
                    // response handler wants us to re-try the request, so use this as the new factory going forwards, and re-try the operation
                    HttpClientFactory = newHttpClientFactoryToRetryRequestWith;
                    using (var newHttpClient = HttpClientFactory.Create())
                    {
                        response.Dispose();
                        response = await PerformRestOperationAsync(newHttpClient, uri, operation, operationContentType, value).ConfigureAwait(false);
                    }
                }

                // handle the response and return it
                ResponseHandler.HandleResponse(response);
                return response;
            }
        }
    }
}
