﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System;

namespace SimpleSyndicate.Web
{
    /// <summary>
    /// <para>
    /// REST client used for interacting with REST resources. If you're dealing with JSON REST resources, it is recommended that you use <see cref="JsonClient"/> instead
    /// as it is designed to simplify working with JSON REST resources -- whilst this client can be used with JSON REST resources, users have to deal with transforming
    /// JSON themselves, which <see cref="JsonClient"/> has built-in.
    /// </para>
    /// <para>
    /// <code language="cs" source="..\..\src\SimpleSyndicate.Web\SimpleSyndicate.Web.Examples\Rest.cs" />
    /// </para>
    /// </summary>
    /// <example>
    /// <code language="cs" source="..\..\src\SimpleSyndicate.Web\SimpleSyndicate.Web.Examples\Rest.cs" />
    /// </example>
    public partial class RestClient : RestClientHttpContent
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RestClient"/> class using the specified <paramref name="responseHandler"/> and <paramref name="httpClientFactory"/>.
        /// </summary>
        /// <param name="responseHandler">An <see cref="IRestResponseHandler"/> that is used to check if a REST request was successful or not.</param>
        /// <param name="httpClientFactory">An <see cref="IHttpClientFactory"/> that is used to create HTTP clients that are used to make REST requests.</param>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="responseHandler"/> or <paramref name="httpClientFactory"/> is <c>null</c>.</exception>
        public RestClient(IRestResponseHandler responseHandler, IHttpClientFactory httpClientFactory)
            : base(responseHandler, httpClientFactory)
        {
            if (responseHandler == null)
            {
                throw new ArgumentNullException(nameof(responseHandler));
            }

            if (httpClientFactory == null)
            {
                throw new ArgumentNullException(nameof(httpClientFactory));
            }
        }
    }
}
