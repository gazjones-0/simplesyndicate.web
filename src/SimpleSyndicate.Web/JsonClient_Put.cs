﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System;
using System.Collections.Specialized;

namespace SimpleSyndicate.Web
{
    /// <content>
    /// Contains PUT-related functionality.
    /// </content>
    public partial class JsonClient
    {
        /// <overloads>
        /// <summary>
        /// Synchronously PUTs JSON, deserializing the response.
        /// </summary>
        /// </overloads>
        /// <summary>
        /// Synchronously PUTs the specified JSON <paramref name="value"/> to the specified <paramref name="address"/>, de-serializing the response to a dynamic object.
        /// </summary>
        /// <param name="address">Uri to the resource.</param>
        /// <param name="value">The JSON to PUT.</param>
        /// <returns>The deserialized PUT's JSON response.</returns>
        public dynamic Put(string address, object value) => Put<dynamic>(new Uri(address), value);

        /// <summary>
        /// Synchronously PUTs the specified JSON <paramref name="value"/> to the URI formed from the <paramref name="baseAddress"/> and <paramref name="path"/>, de-serializing the response to a dynamic object.
        /// </summary>
        /// <param name="baseAddress">Base address.</param>
        /// <param name="path">Path to resource.</param>
        /// <param name="value">The JSON to PUT.</param>
        /// <returns>The deserialized PUT's JSON response from the URI formed from the <paramref name="baseAddress"/> and <paramref name="path"/>.</returns>
        public dynamic Put(string baseAddress, string path, object value)
        {
            var response = PutAsync<dynamic>(baseAddress, path, value);
            response.Wait();
            return response.Result;
        }

        /// <summary>
        /// Synchronously PUTs the specified JSON <paramref name="value"/> to the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and single parameter, de-serializing the response to a dynamic object.
        /// </summary>
        /// <param name="baseAddress">Base address.</param>
        /// <param name="path">Path to resource.</param>
        /// <param name="parameterName">Name of the parameter.</param>
        /// <param name="parameterValue">Value of the parameter.</param>
        /// <param name="value">The JSON to PUT.</param>
        /// <returns>The deserialized PUT's JSON response from the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and single parameter.</returns>
        public dynamic Put(string baseAddress, string path, string parameterName, string parameterValue, object value)
        {
            var response = PutAsync<dynamic>(baseAddress, path, parameterName, parameterValue, value);
            response.Wait();
            return response.Result;
        }

        /// <summary>
        /// Synchronously PUTs the specified JSON <paramref name="value"/> to the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and two parameters, de-serializing the response to a dynamic object.
        /// </summary>
        /// <param name="baseAddress">Base address.</param>
        /// <param name="path">Path to resource.</param>
        /// <param name="parameterName1">Name of the first parameter.</param>
        /// <param name="parameterValue1">Value of the first parameter.</param>
        /// <param name="parameterName2">Name of the second parameter.</param>
        /// <param name="parameterValue2">Value of the second parameter.</param>
        /// <param name="value">The JSON to PUT.</param>
        /// <returns>The deserialized PUT's JSON response from the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and two parameters.</returns>
        public dynamic Put(string baseAddress, string path, string parameterName1, string parameterValue1, string parameterName2, string parameterValue2, object value)
        {
            var response = PutAsync<dynamic>(baseAddress, path, parameterName1, parameterValue1, parameterName2, parameterValue2, value);
            response.Wait();
            return response.Result;
        }

        /// <summary>
        /// Synchronously PUTs the specified JSON <paramref name="value"/> to the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and three parameters, de-serializing the response to a dynamic object.
        /// </summary>
        /// <param name="baseAddress">Base address.</param>
        /// <param name="path">Path to resource.</param>
        /// <param name="parameterName1">Name of the first parameter.</param>
        /// <param name="parameterValue1">Value of the first parameter.</param>
        /// <param name="parameterName2">Name of the second parameter.</param>
        /// <param name="parameterValue2">Value of the second parameter.</param>
        /// <param name="parameterName3">Name of the third parameter.</param>
        /// <param name="parameterValue3">Value of the third parameter.</param>
        /// <param name="value">The JSON to PUT.</param>
        /// <returns>The deserialized PUT's JSON response from the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and three parameters.</returns>
        public dynamic Put(string baseAddress, string path, string parameterName1, string parameterValue1, string parameterName2, string parameterValue2, string parameterName3, string parameterValue3, object value)
        {
            var response = PutAsync<dynamic>(baseAddress, path, parameterName1, parameterValue1, parameterName2, parameterValue2, parameterName3, parameterValue3, value);
            response.Wait();
            return response.Result;
        }

        /// <summary>
        /// Synchronously PUTs the specified JSON <paramref name="value"/> to the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and parameter collection, de-serializing the response to a dynamic object.
        /// </summary>
        /// <param name="baseAddress">Base address.</param>
        /// <param name="path">Path to resource.</param>
        /// <param name="parameters">Collection of parameters.</param>
        /// <param name="value">The JSON to PUT.</param>
        /// <returns>The deserialized PUT's JSON response from the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and parameter collection.</returns>
        public dynamic Put(string baseAddress, string path, NameValueCollection parameters, object value)
        {
            var response = PutAsync<dynamic>(baseAddress, path, parameters, value);
            response.Wait();
            return response.Result;
        }

        /// <summary>
        /// Synchronously PUTs the specified JSON <paramref name="value"/> to the specified <paramref name="uri"/>, de-serializing the response to a dynamic object.
        /// </summary>
        /// <param name="uri">Uri to the resource.</param>
        /// <param name="value">The JSON to PUT.</param>
        /// <returns>The deserialized PUT's JSON response from the specified <paramref name="uri"/>.</returns>
        public dynamic Put(Uri uri, object value)
        {
            var response = PutAsync<dynamic>(uri, value);
            response.Wait();
            return response.Result;
        }

        /// <summary>
        /// Synchronously PUTs the specified JSON <paramref name="value"/> to the specified <paramref name="address"/>, de-serializing the response to <typeparamref name="TJson"/>.
        /// </summary>
        /// <typeparam name="TJson">The type to deserialize the JSON to.</typeparam>
        /// <param name="address">Uri to the resource.</param>
        /// <param name="value">The JSON to PUT.</param>
        /// <returns>The deserialized PUT's JSON response.</returns>
        public TJson Put<TJson>(string address, object value) => Put<TJson>(new Uri(address), value);

        /// <summary>
        /// Synchronously PUTs the specified JSON <paramref name="value"/> to the URI formed from the <paramref name="baseAddress"/> and <paramref name="path"/>, de-serializing the response to <typeparamref name="TJson"/>.
        /// </summary>
        /// <typeparam name="TJson">The type to deserialize the JSON to.</typeparam>
        /// <param name="baseAddress">Base address.</param>
        /// <param name="path">Path to resource.</param>
        /// <param name="value">The JSON to PUT.</param>
        /// <returns>The deserialized PUT's JSON response from the URI formed from the <paramref name="baseAddress"/> and <paramref name="path"/>.</returns>
        public TJson Put<TJson>(string baseAddress, string path, object value)
        {
            var response = PutAsync<TJson>(baseAddress, path, value);
            response.Wait();
            return response.Result;
        }

        /// <summary>
        /// Synchronously PUTs the specified JSON <paramref name="value"/> to the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and single parameter, de-serializing the response to <typeparamref name="TJson"/>.
        /// </summary>
        /// <typeparam name="TJson">The type to deserialize the JSON to.</typeparam>
        /// <param name="baseAddress">Base address.</param>
        /// <param name="path">Path to resource.</param>
        /// <param name="parameterName">Name of the parameter.</param>
        /// <param name="parameterValue">Value of the parameter.</param>
        /// <param name="value">The JSON to PUT.</param>
        /// <returns>The deserialized PUT's JSON response from the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and single parameter.</returns>
        public TJson Put<TJson>(string baseAddress, string path, string parameterName, string parameterValue, object value)
        {
            var response = PutAsync<TJson>(baseAddress, path, parameterName, parameterValue, value);
            response.Wait();
            return response.Result;
        }

        /// <summary>
        /// Synchronously PUTs the specified JSON <paramref name="value"/> to the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and two parameters, de-serializing the response to <typeparamref name="TJson"/>.
        /// </summary>
        /// <typeparam name="TJson">The type to deserialize the JSON to.</typeparam>
        /// <param name="baseAddress">Base address.</param>
        /// <param name="path">Path to resource.</param>
        /// <param name="parameterName1">Name of the first parameter.</param>
        /// <param name="parameterValue1">Value of the first parameter.</param>
        /// <param name="parameterName2">Name of the second parameter.</param>
        /// <param name="parameterValue2">Value of the second parameter.</param>
        /// <param name="value">The JSON to PUT.</param>
        /// <returns>The deserialized PUT's JSON response from the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and two parameters.</returns>
        public TJson Put<TJson>(string baseAddress, string path, string parameterName1, string parameterValue1, string parameterName2, string parameterValue2, object value)
        {
            var response = PutAsync<TJson>(baseAddress, path, parameterName1, parameterValue1, parameterName2, parameterValue2, value);
            response.Wait();
            return response.Result;
        }

        /// <summary>
        /// Synchronously PUTs the specified JSON <paramref name="value"/> to the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and three parameters, de-serializing the response to <typeparamref name="TJson"/>.
        /// </summary>
        /// <typeparam name="TJson">The type to deserialize the JSON to.</typeparam>
        /// <param name="baseAddress">Base address.</param>
        /// <param name="path">Path to resource.</param>
        /// <param name="parameterName1">Name of the first parameter.</param>
        /// <param name="parameterValue1">Value of the first parameter.</param>
        /// <param name="parameterName2">Name of the second parameter.</param>
        /// <param name="parameterValue2">Value of the second parameter.</param>
        /// <param name="parameterName3">Name of the third parameter.</param>
        /// <param name="parameterValue3">Value of the third parameter.</param>
        /// <param name="value">The JSON to PUT.</param>
        /// <returns>The deserialized PUT's JSON response from the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and three parameters.</returns>
        public TJson Put<TJson>(string baseAddress, string path, string parameterName1, string parameterValue1, string parameterName2, string parameterValue2, string parameterName3, string parameterValue3, object value)
        {
            var response = PutAsync<TJson>(baseAddress, path, parameterName1, parameterValue1, parameterName2, parameterValue2, parameterName3, parameterValue3, value);
            response.Wait();
            return response.Result;
        }

        /// <summary>
        /// Synchronously PUTs the specified JSON <paramref name="value"/> to the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and parameter collection, de-serializing the response to <typeparamref name="TJson"/>.
        /// </summary>
        /// <typeparam name="TJson">The type to deserialize the JSON to.</typeparam>
        /// <param name="baseAddress">Base address.</param>
        /// <param name="path">Path to resource.</param>
        /// <param name="parameters">Collection of parameters.</param>
        /// <param name="value">The JSON to PUT.</param>
        /// <returns>The deserialized PUT's JSON response from the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and parameter collection.</returns>
        public TJson Put<TJson>(string baseAddress, string path, NameValueCollection parameters, object value)
        {
            var response = PutAsync<TJson>(baseAddress, path, parameters, value);
            response.Wait();
            return response.Result;
        }

        /// <summary>
        /// Synchronously PUTs the specified JSON <paramref name="value"/> to the specified <paramref name="uri"/>, de-serializing the response to <typeparamref name="TJson"/>.
        /// </summary>
        /// <typeparam name="TJson">The type to deserialize the JSON to.</typeparam>
        /// <param name="uri">Uri to the resource.</param>
        /// <param name="value">The JSON to PUT.</param>
        /// <returns>The deserialized PUT's JSON response from the specified <paramref name="uri"/>.</returns>
        public TJson Put<TJson>(Uri uri, object value)
        {
            var response = PutAsync<TJson>(uri, value);
            response.Wait();
            return response.Result;
        }
    }
}
