﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System;
using System.Collections.Specialized;

namespace SimpleSyndicate.Web
{
    /// <content>
    /// Contains content GET-related functionality.
    /// </content>
    public partial class RestClientBase
    {
        /// <overloads>
        /// <summary>
        /// Makes a synchronous GET request, returning all the content of the response as a string.
        /// </summary>
        /// </overloads>
        /// <summary>
        /// Makes a synchronous GET request to the specified <paramref name="address"/>, returning all the content of the response as a string.
        /// </summary>
        /// <param name="address">Path to the resource.</param>
        /// <returns>The GET's response.</returns>
        public string GetContent(string address) => GetContent(new Uri(address));

        /// <summary>
        /// Makes a synchronous GET request to the URI formed from the <paramref name="baseAddress"/> and the <paramref name="path"/>, returning all the content of the response as a string.
        /// </summary>
        /// <param name="baseAddress">Base address.</param>
        /// <param name="path">Path to resource.</param>
        /// <returns>The GET's response from the URI formed from the <paramref name="baseAddress"/> and the <paramref name="path"/>.</returns>
        public string GetContent(string baseAddress, string path)
        {
            var response = GetContentAsync(baseAddress, path);
            response.Wait();
            return response.Result;
        }

        /// <summary>
        /// Makes a synchronous GET request to the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and single parameter, returning all the content of the response as a string.
        /// </summary>
        /// <param name="baseAddress">Base address.</param>
        /// <param name="path">Path to resource.</param>
        /// <param name="parameterName">Name of the parameter.</param>
        /// <param name="parameterValue">Value of the parameter.</param>
        /// <returns>The GET's response from the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and single parameter.</returns>
        public string GetContent(string baseAddress, string path, string parameterName, string parameterValue)
        {
            var response = GetContentAsync(baseAddress, path, parameterName, parameterValue);
            response.Wait();
            return response.Result;
        }

        /// <summary>
        /// Makes a synchronous GET request to the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and two parameters, returning all the content of the response as a string.
        /// </summary>
        /// <param name="baseAddress">Base address.</param>
        /// <param name="path">Path to resource.</param>
        /// <param name="parameterName1">Name of the first parameter.</param>
        /// <param name="parameterValue1">Value of the first parameter.</param>
        /// <param name="parameterName2">Name of the second parameter.</param>
        /// <param name="parameterValue2">Value of the second parameter.</param>
        /// <returns>The GET's response from the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and two parameters.</returns>
        public string GetContent(string baseAddress, string path, string parameterName1, string parameterValue1, string parameterName2, string parameterValue2)
        {
            var response = GetContentAsync(baseAddress, path, parameterName1, parameterValue1, parameterName2, parameterValue2);
            response.Wait();
            return response.Result;
        }

        /// <summary>
        /// Makes a synchronous GET request to the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and three parameters, returning all the content of the response as a string.
        /// </summary>
        /// <param name="baseAddress">Base address.</param>
        /// <param name="path">Path to resource.</param>
        /// <param name="parameterName1">Name of the first parameter.</param>
        /// <param name="parameterValue1">Value of the first parameter.</param>
        /// <param name="parameterName2">Name of the second parameter.</param>
        /// <param name="parameterValue2">Value of the second parameter.</param>
        /// <param name="parameterName3">Name of the third parameter.</param>
        /// <param name="parameterValue3">Value of the third parameter.</param>
        /// <returns>The GET's response from the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and three parameters.</returns>
        public string GetContent(string baseAddress, string path, string parameterName1, string parameterValue1, string parameterName2, string parameterValue2, string parameterName3, string parameterValue3)
        {
            var response = GetContentAsync(baseAddress, path, parameterName1, parameterValue1, parameterName2, parameterValue2, parameterName3, parameterValue3);
            response.Wait();
            return response.Result;
        }

        /// <summary>
        /// Makes a synchronous GET request to the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and parameter collection, returning all the content of the response as a string.
        /// </summary>
        /// <param name="baseAddress">Base address.</param>
        /// <param name="path">Path to resource.</param>
        /// <param name="parameters">Collection of parameters.</param>
        /// <returns>The GET's response from the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and parameter collection.</returns>
        public string GetContent(string baseAddress, string path, NameValueCollection parameters)
        {
            var response = GetContentAsync(baseAddress, path, parameters);
            response.Wait();
            return response.Result;
        }

        /// <summary>
        /// Makes a synchronous GET request to the specified <paramref name="uri"/>, returning all the content of the response as a string.
        /// </summary>
        /// <param name="uri">Uri to the resource.</param>
        /// <returns>The GET's response.</returns>
        public string GetContent(Uri uri)
        {
            var response = GetContentAsync(uri);
            response.Wait();
            return response.Result;
        }
    }
}
