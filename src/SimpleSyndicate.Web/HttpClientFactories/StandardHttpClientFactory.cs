﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;

namespace SimpleSyndicate.Web.HttpClientFactories
{
    /// <summary>
    /// Factory class for creating <see cref="HttpClient"/>s.
    /// </summary>
    public class StandardHttpClientFactory : IHttpClientFactory
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="StandardHttpClientFactory"/> class.
        /// </summary>
        public StandardHttpClientFactory()
        {
            AuthenticationHeaderValue = null;
            EnableTls11 = true;
            EnableTls12 = true;
            CertificateValidation = CertificateValidation.ValidOnly;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="StandardHttpClientFactory"/> class using the specified <paramref name="authenticationHeaderValue"/>.
        /// </summary>
        /// <param name="authenticationHeaderValue">The authentication information.</param>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="authenticationHeaderValue"/> is <c>null</c>.</exception>
        public StandardHttpClientFactory(AuthenticationHeaderValue authenticationHeaderValue)
            : this() => AuthenticationHeaderValue = authenticationHeaderValue ?? throw new ArgumentNullException(nameof(authenticationHeaderValue));

        /// <summary>
        /// Gets or sets the <see cref="AuthenticationHeaderValue"/> that will be set when <see cref="HttpClient"/>s are created.
        /// </summary>
        /// <value>The authentication information.</value>
        public AuthenticationHeaderValue AuthenticationHeaderValue { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether TLS 1.1 should be enabled if it isn't already; the default is to enable it.
        /// </summary>
        /// <value><c>true</c> to indicate TLS 1.1. should be enabled if it isn't already; <c>false</c> otherwise.</value>
        /// <remarks>
        /// Trying to access a service that requires TLS 1.1 from a device that doesn't have it enabled can result in unintuitive
        /// exceptions being thrown so the default is to enable TLS 1.1 if it isn't already, and so avoiding this situation.
        /// </remarks>
        public bool EnableTls11 { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether TLS 1.2 should be enabled if it isn't already; the default is to enable it.
        /// </summary>
        /// <value><c>true</c> to indicate TLS 1.2. should be enabled if it isn't already; <c>false</c> otherwise.</value>
        /// <remarks>
        /// Trying to access a service that requires TLS 1.2 from a device that doesn't have it enabled can result in unintuitive
        /// exceptions being thrown so the default is to enable TLS 1.2 if it isn't already, and so avoiding this situation.
        /// </remarks>
        public bool EnableTls12 { get; set; }

        /// <summary>
        /// Gets or sets a value indicating the certificate validation to perform; the default is <c>CertificateValidation.ValidOnly</c>.
        /// </summary>
        /// <value><see cref="CertificateValidation"/> option.</value>
        public CertificateValidation CertificateValidation { get; set; }

        /// <summary>
        /// Certificate validation callback that returns <c>true</c> if the certificate is valid or self-signed.
        /// </summary>
        /// <param name="sender">Calling object.</param>
        /// <param name="certificate">Certificate to validate.</param>
        /// <param name="chain">Certificate chain.</param>
        /// <param name="sslPolicyErrors">SSL errors.</param>
        /// <returns><c>true</c> if the certificate is valid or self-signed; <c>false</c> otherwise.</returns>
        public static bool RemoteCertificateValidationCallbackValidOrSelfSigned(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            if (sslPolicyErrors == SslPolicyErrors.None)
            {
                return true;
            }

            // a self-signed certificate will cause an error in the certificate chain, so check for that
            if ((sslPolicyErrors & SslPolicyErrors.RemoteCertificateChainErrors) != 0)
            {
                if (chain != null && chain.ChainStatus != null)
                {
                    // check through the whole certificate chain
                    foreach (var status in chain.ChainStatus)
                    {
                        // if it's a self-signed certifcate with an untrusted root, continue on to the next one in the chain
                        if ((certificate.Subject == certificate.Issuer)
                            && (status.Status == X509ChainStatusFlags.UntrustedRoot))
                        {
                            continue;
                        }

                        // not self-signed, but if there's any other error the certificate is invalid for some other reason
                        if (status.Status != X509ChainStatusFlags.NoError)
                        {
                            return false;
                        }
                    }
                }

                // only error in the chain is a self-signed certificate, so certificate is valid
                return true;
            }

            // something other than a chain error, certificate not valid
            return false;
        }

        /// <summary>
        /// Certificate validation callback that always returns <c>true</c>.
        /// </summary>
        /// <param name="sender">Calling object.</param>
        /// <param name="certificate">Certificate to validate.</param>
        /// <param name="chain">Certificate chain.</param>
        /// <param name="sslPolicyErrors">SSL errors.</param>
        /// <returns><c>true</c> in all cases.</returns>
        public static bool RemoteCertificateValidationCallbackAcceptAny(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }

        /// <summary>
        /// Creates a new <see cref="HttpClient"/> object, which is set to use default credentials when making requests.
        /// </summary>
        /// <returns>An <see cref="HttpClient"/> object.</returns>
        public HttpClient Create()
        {
            if (EnableTls11)
            {
                if ((ServicePointManager.SecurityProtocol & SecurityProtocolType.Tls11) != SecurityProtocolType.Tls11)
                {
                    ServicePointManager.SecurityProtocol = ServicePointManager.SecurityProtocol | SecurityProtocolType.Tls11;
                }
            }

            if (EnableTls12)
            {
                if ((ServicePointManager.SecurityProtocol & SecurityProtocolType.Tls12) != SecurityProtocolType.Tls12)
                {
                    ServicePointManager.SecurityProtocol = ServicePointManager.SecurityProtocol | SecurityProtocolType.Tls12;
                }
            }

            var handler = new HttpClientHandler()
            {
                UseDefaultCredentials = true
            };

            switch (CertificateValidation)
            {
                case CertificateValidation.ValidOrSelfSigned:
                    handler.ServerCertificateCustomValidationCallback += RemoteCertificateValidationCallbackValidOrSelfSigned;
                    break;

                case CertificateValidation.AcceptAny:
                    handler.ServerCertificateCustomValidationCallback += RemoteCertificateValidationCallbackAcceptAny;
                    break;
            }

            var httpClient = new HttpClient(handler, true);
            if (AuthenticationHeaderValue != null)
            {
                httpClient.DefaultRequestHeaders.Authorization = AuthenticationHeaderValue;
            }

            return httpClient;
        }
    }
}
