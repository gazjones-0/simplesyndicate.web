﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

namespace SimpleSyndicate.Web.HttpClientFactories
{
    /// <summary>
    /// Certificate validation option.
    /// </summary>
    public enum CertificateValidation
    {
        /// <summary>
        /// Certificates must be valid.
        /// </summary>
        ValidOnly = 0,

        /// <summary>
        /// Certificates must be valid or self-signed.
        /// </summary>
        ValidOrSelfSigned = 1,

        /// <summary>
        /// Accept any certificate, including invalid ones.
        /// </summary>
        AcceptAny = 2
    }
}
