﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System;
using System.Net.Http.Headers;

namespace SimpleSyndicate.Web.HttpClientFactories
{
    /// <summary>
    /// Factory class for creating <see cref="IHttpClientFactory"/>.
    /// </summary>
    public static class IHttpClientFactoryFactory
    {
        /// <summary>
        /// Creates a new <see cref="IHttpClientFactory"/> that is identical to the specificed <paramref name="factory"/>.
        /// </summary>
        /// <param name="factory">Factory to duplicate.</param>
        /// <returns>A new <see cref="IHttpClientFactory"/>.</returns>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="factory"/> is <c>null</c>.</exception>
        /// <exception cref="ArgumentException">Thrown when <paramref name="factory"/> is an unsupported <see cref="IHttpClientFactory"/> implementation.</exception>
        public static IHttpClientFactory Create(IHttpClientFactory factory)
        {
            if (factory == null)
            {
                throw new ArgumentNullException(nameof(factory));
            }

            if (factory is StandardHttpClientFactory)
            {
                return new StandardHttpClientFactory()
                {
                    AuthenticationHeaderValue = ((StandardHttpClientFactory)factory).AuthenticationHeaderValue,
                    CertificateValidation = ((StandardHttpClientFactory)factory).CertificateValidation,
                    EnableTls11 = ((StandardHttpClientFactory)factory).EnableTls11,
                    EnableTls12 = ((StandardHttpClientFactory)factory).EnableTls12
                };
            }

            throw new ArgumentException("Unsupported IHttpClientFactory implementation", nameof(factory));
        }

        /// <summary>
        /// Creates a new <see cref="IHttpClientFactory"/> that is identical to the specificed <paramref name="factory"/>.
        /// </summary>
        /// <param name="factory">Factory to duplicate.</param>
        /// <param name="authenticationHeaderValue">Authentication header value.</param>
        /// <returns>A new <see cref="IHttpClientFactory"/>.</returns>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="factory"/> is <c>null</c>.</exception>
        /// <exception cref="ArgumentException">Thrown when <paramref name="factory"/> is an unsupported <see cref="IHttpClientFactory"/> implementation.</exception>
        public static IHttpClientFactory Create(IHttpClientFactory factory, AuthenticationHeaderValue authenticationHeaderValue)
        {
            var newFactory = Create(factory);
            newFactory.AuthenticationHeaderValue = authenticationHeaderValue;
            return newFactory;
        }
    }
}
