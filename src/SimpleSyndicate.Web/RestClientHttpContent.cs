﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System;
using System.Net.Http;

namespace SimpleSyndicate.Web
{
    /// <summary>
    /// REST client used for interacting with REST resources, extending the base JSON functionality to support <see cref="HttpContent"/>.
    /// </summary>
    public abstract partial class RestClientHttpContent : RestClientBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RestClientHttpContent"/> class using the specified <paramref name="responseHandler"/> and <paramref name="httpClientFactory"/>.
        /// </summary>
        /// <param name="responseHandler">An <see cref="IRestResponseHandler"/> that is used to check if a REST request was successful or not.</param>
        /// <param name="httpClientFactory">An <see cref="IHttpClientFactory"/> that is used to create HTTP clients that are used to make REST requests.</param>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="responseHandler"/> or <paramref name="httpClientFactory"/> is <c>null</c>.</exception>
        protected RestClientHttpContent(IRestResponseHandler responseHandler, IHttpClientFactory httpClientFactory)
            : base(responseHandler, httpClientFactory)
        {
            if (responseHandler == null)
            {
                throw new ArgumentNullException(nameof(responseHandler));
            }

            if (httpClientFactory == null)
            {
                throw new ArgumentNullException(nameof(httpClientFactory));
            }
        }
    }
}
