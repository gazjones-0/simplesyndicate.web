﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System;
using System.Collections.Specialized;
using System.Threading.Tasks;

namespace SimpleSyndicate.Web
{
    /// <content>
    /// Contains async POST-related functionality.
    /// </content>
    public partial class JsonClient
    {
        /// <overloads>
        /// <summary>
        /// Asynchronously POSTs JSON, deserializing the response.
        /// </summary>
        /// </overloads>
        /// <summary>
        /// Asynchronously POSTS the specified <paramref name="value"/> to the specified <paramref name="address"/>, de-serializing the response to a dynamic object.
        /// </summary>
        /// <param name="address">Uri to the resource.</param>
        /// <param name="value">The JSON to POST.</param>
        /// <returns>The deserialized POST's JSON response.</returns>
        public async Task<dynamic> PostAsync(string address, object value) => await PostAsync<dynamic>(new Uri(address), value).ConfigureAwait(false);

        /// <summary>
        /// Asynchronously POSTs the specified JSON <paramref name="value"/> to the URI formed from the <paramref name="baseAddress"/> and <paramref name="path"/>, de-serializing the response to a dynamic object.
        /// </summary>
        /// <param name="baseAddress">Base address.</param>
        /// <param name="path">Path to resource.</param>
        /// <param name="value">The JSON to POST.</param>
        /// <returns>The deserialized POST's JSON response from the URI formed from the <paramref name="baseAddress"/> and <paramref name="path"/>.</returns>
        public async Task<dynamic> PostAsync(string baseAddress, string path, object value)
        {
            var parameters = new NameValueCollection();
            return await PostAsync<dynamic>(baseAddress, path, parameters, value).ConfigureAwait(false);
        }

        /// <summary>
        /// Asynchronously POSTs the specified JSON <paramref name="value"/> to the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and single parameter, de-serializing the response to a dynamic object.
        /// </summary>
        /// <param name="baseAddress">Base address.</param>
        /// <param name="path">Path to resource.</param>
        /// <param name="parameterName">Name of the parameter.</param>
        /// <param name="parameterValue">Value of the parameter.</param>
        /// <param name="value">The JSON to POST.</param>
        /// <returns>The deserialized POST's JSON response from the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and single parameter.</returns>
        public async Task<dynamic> PostAsync(string baseAddress, string path, string parameterName, string parameterValue, object value)
        {
            var parameters = new NameValueCollection
            {
                { parameterName, parameterValue }
            };
            return await PostAsync<dynamic>(baseAddress, path, parameters, value).ConfigureAwait(false);
        }

        /// <summary>
        /// Asynchronously POSTs the specified JSON <paramref name="value"/> to the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and two parameters, de-serializing the response to a dynamic object.
        /// </summary>
        /// <param name="baseAddress">Base address.</param>
        /// <param name="path">Path to resource.</param>
        /// <param name="parameterName1">Name of the first parameter.</param>
        /// <param name="parameterValue1">Value of the first parameter.</param>
        /// <param name="parameterName2">Name of the second parameter.</param>
        /// <param name="parameterValue2">Value of the second parameter.</param>
        /// <param name="value">The JSON to POST.</param>
        /// <returns>The deserialized POST's JSON response from the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and two parameters.</returns>
        public async Task<dynamic> PostAsync(string baseAddress, string path, string parameterName1, string parameterValue1, string parameterName2, string parameterValue2, object value)
        {
            var parameters = new NameValueCollection
            {
                { parameterName1, parameterValue1 },
                { parameterName2, parameterValue2 }
            };
            return await PostAsync<dynamic>(baseAddress, path, parameters, value).ConfigureAwait(false);
        }

        /// <summary>
        /// Asynchronously POSTs the specified JSON <paramref name="value"/> to the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and three parameters, de-serializing the response to a dynamic object.
        /// </summary>
        /// <param name="baseAddress">Base address.</param>
        /// <param name="path">Path to resource.</param>
        /// <param name="parameterName1">Name of the first parameter.</param>
        /// <param name="parameterValue1">Value of the first parameter.</param>
        /// <param name="parameterName2">Name of the second parameter.</param>
        /// <param name="parameterValue2">Value of the second parameter.</param>
        /// <param name="parameterName3">Name of the third parameter.</param>
        /// <param name="parameterValue3">Value of the third parameter.</param>
        /// <param name="value">The JSON to POST.</param>
        /// <returns>The deserialized POST's JSON response from the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and three parameters.</returns>
        public async Task<dynamic> PostAsync(string baseAddress, string path, string parameterName1, string parameterValue1, string parameterName2, string parameterValue2, string parameterName3, string parameterValue3, object value)
        {
            var parameters = new NameValueCollection
            {
                { parameterName1, parameterValue1 },
                { parameterName2, parameterValue2 },
                { parameterName3, parameterValue3 }
            };
            return await PostAsync<dynamic>(baseAddress, path, parameters, value).ConfigureAwait(false);
        }

        /// <summary>
        /// Asynchronously POSTs the specified JSON <paramref name="value"/> to the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and parameter collection, de-serializing the response to a dynamic object.
        /// </summary>
        /// <param name="baseAddress">Base address.</param>
        /// <param name="path">Path to resource.</param>
        /// <param name="parameters">Collection of parameters.</param>
        /// <param name="value">The JSON to POST.</param>
        /// <returns>The deserialized POST's JSON response from the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and parameter collection.</returns>
        public async Task<dynamic> PostAsync(string baseAddress, string path, NameValueCollection parameters, object value) => await PostAsync<dynamic>(UriHelpers.BuildUri(baseAddress, path, parameters), value).ConfigureAwait(false);

        /// <summary>
        /// Asynchronously POSTs the specified JSON <paramref name="value"/> to the specified <paramref name="uri"/>, de-serializing the response to a dynamic object.
        /// </summary>
        /// <param name="uri">Uri to the resource.</param>
        /// <param name="value">The JSON to POST.</param>
        /// <returns>The deserialized POST's JSON response from the specified <paramref name="uri"/>.</returns>
        public async Task<dynamic> PostAsync(Uri uri, object value) => await PostAsync<dynamic>(uri, value).ConfigureAwait(false);

        /// <summary>
        /// Asynchronously POSTS the specified <paramref name="value"/> to the specified <paramref name="address"/>, de-serializing the response to <typeparamref name="TJson"/>.
        /// </summary>
        /// <typeparam name="TJson">The type to deserialize the JSON to.</typeparam>
        /// <param name="address">Uri to the resource.</param>
        /// <param name="value">The JSON to POST.</param>
        /// <returns>The deserialized POST's JSON response.</returns>
        public async Task<TJson> PostAsync<TJson>(string address, object value) => await PostAsync<TJson>(new Uri(address), value).ConfigureAwait(false);

        /// <summary>
        /// Asynchronously POSTs the specified JSON <paramref name="value"/> to the URI formed from the <paramref name="baseAddress"/> and <paramref name="path"/>, de-serializing the response to <typeparamref name="TJson"/>.
        /// </summary>
        /// <typeparam name="TJson">The type to deserialize the JSON to.</typeparam>
        /// <param name="baseAddress">Base address.</param>
        /// <param name="path">Path to resource.</param>
        /// <param name="value">The JSON to POST.</param>
        /// <returns>The deserialized POST's JSON response from the URI formed from the <paramref name="baseAddress"/> and <paramref name="path"/>.</returns>
        public async Task<TJson> PostAsync<TJson>(string baseAddress, string path, object value)
        {
            var parameters = new NameValueCollection();
            return await PostAsync<TJson>(baseAddress, path, parameters, value).ConfigureAwait(false);
        }

        /// <summary>
        /// Asynchronously POSTs the specified JSON <paramref name="value"/> to the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and single parameter, de-serializing the response to <typeparamref name="TJson"/>.
        /// </summary>
        /// <typeparam name="TJson">The type to deserialize the JSON to.</typeparam>
        /// <param name="baseAddress">Base address.</param>
        /// <param name="path">Path to resource.</param>
        /// <param name="parameterName">Name of the parameter.</param>
        /// <param name="parameterValue">Value of the parameter.</param>
        /// <param name="value">The JSON to POST.</param>
        /// <returns>The deserialized POST's JSON response from the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and single parameter.</returns>
        public async Task<TJson> PostAsync<TJson>(string baseAddress, string path, string parameterName, string parameterValue, object value)
        {
            var parameters = new NameValueCollection
            {
                { parameterName, parameterValue }
            };
            return await PostAsync<TJson>(baseAddress, path, parameters, value).ConfigureAwait(false);
        }

        /// <summary>
        /// Asynchronously POSTs the specified JSON <paramref name="value"/> to the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and two parameters, de-serializing the response to <typeparamref name="TJson"/>.
        /// </summary>
        /// <typeparam name="TJson">The type to deserialize the JSON to.</typeparam>
        /// <param name="baseAddress">Base address.</param>
        /// <param name="path">Path to resource.</param>
        /// <param name="parameterName1">Name of the first parameter.</param>
        /// <param name="parameterValue1">Value of the first parameter.</param>
        /// <param name="parameterName2">Name of the second parameter.</param>
        /// <param name="parameterValue2">Value of the second parameter.</param>
        /// <param name="value">The JSON to POST.</param>
        /// <returns>The deserialized POST's JSON response from the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and two parameters.</returns>
        public async Task<TJson> PostAsync<TJson>(string baseAddress, string path, string parameterName1, string parameterValue1, string parameterName2, string parameterValue2, object value)
        {
            var parameters = new NameValueCollection
            {
                { parameterName1, parameterValue1 },
                { parameterName2, parameterValue2 }
            };
            return await PostAsync<TJson>(baseAddress, path, parameters, value).ConfigureAwait(false);
        }

        /// <summary>
        /// Asynchronously POSTs the specified JSON <paramref name="value"/> to the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and three parameters, de-serializing the response to <typeparamref name="TJson"/>.
        /// </summary>
        /// <typeparam name="TJson">The type to deserialize the JSON to.</typeparam>
        /// <param name="baseAddress">Base address.</param>
        /// <param name="path">Path to resource.</param>
        /// <param name="parameterName1">Name of the first parameter.</param>
        /// <param name="parameterValue1">Value of the first parameter.</param>
        /// <param name="parameterName2">Name of the second parameter.</param>
        /// <param name="parameterValue2">Value of the second parameter.</param>
        /// <param name="parameterName3">Name of the third parameter.</param>
        /// <param name="parameterValue3">Value of the third parameter.</param>
        /// <param name="value">The JSON to POST.</param>
        /// <returns>The deserialized POST's JSON response from the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and three parameters.</returns>
        public async Task<TJson> PostAsync<TJson>(string baseAddress, string path, string parameterName1, string parameterValue1, string parameterName2, string parameterValue2, string parameterName3, string parameterValue3, object value)
        {
            var parameters = new NameValueCollection
            {
                { parameterName1, parameterValue1 },
                { parameterName2, parameterValue2 },
                { parameterName3, parameterValue3 }
            };
            return await PostAsync<TJson>(baseAddress, path, parameters, value).ConfigureAwait(false);
        }

        /// <summary>
        /// Asynchronously POSTs the specified JSON <paramref name="value"/> to the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and parameter collection, de-serializing the response to <typeparamref name="TJson"/>.
        /// </summary>
        /// <typeparam name="TJson">The type to deserialize the JSON to.</typeparam>
        /// <param name="baseAddress">Base address.</param>
        /// <param name="path">Path to resource.</param>
        /// <param name="parameters">Collection of parameters.</param>
        /// <param name="value">The JSON to POST.</param>
        /// <returns>The deserialized POST's JSON response from the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and parameter collection.</returns>
        public async Task<TJson> PostAsync<TJson>(string baseAddress, string path, NameValueCollection parameters, object value) => await PostAsync<TJson>(UriHelpers.BuildUri(baseAddress, path, parameters), value).ConfigureAwait(false);

        /// <summary>
        /// Asynchronously POSTs the specified JSON <paramref name="value"/> to the specified <paramref name="uri"/>, de-serializing the response to <typeparamref name="TJson"/>.
        /// </summary>
        /// <typeparam name="TJson">The type to deserialize the JSON to.</typeparam>
        /// <param name="uri">Uri to the resource.</param>
        /// <param name="value">The JSON to POST.</param>
        /// <returns>The deserialized POST's JSON response from the specified <paramref name="uri"/>.</returns>
        public async Task<TJson> PostAsync<TJson>(Uri uri, object value) => await PerformJsonOperationAsync<TJson>(uri, RestOperation.Post, value).ConfigureAwait(false);
    }
}
