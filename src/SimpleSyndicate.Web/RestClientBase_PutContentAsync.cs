﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System;
using System.Collections.Specialized;
using System.Threading.Tasks;

namespace SimpleSyndicate.Web
{
    /// <content>
    /// Contains async content PUT-related functionality.
    /// </content>
    public partial class RestClientBase
    {
        /// <overloads>
        /// <summary>
        /// Asynchronously PUTs, returning all the content of the response as a string.
        /// </summary>
        /// </overloads>
        /// <summary>
        /// Asynchronously PUTs the specified JSON <paramref name="value"/> to the specified <paramref name="address"/>, returning all the content of the response as a string.
        /// </summary>
        /// <param name="address">Uri to the resource.</param>
        /// <param name="value">The JSON to PUT.</param>
        /// <returns>The PUT's response.</returns>
        public async Task<string> PutContentAsync(string address, object value) => await PutContentAsync(new Uri(address), value).ConfigureAwait(false);

        /// <summary>
        /// Asynchronously PUTs the specified JSON <paramref name="value"/> to the URI formed from the <paramref name="baseAddress"/> and <paramref name="path"/>, returning all the content of the response as a string.
        /// </summary>
        /// <param name="baseAddress">Base address.</param>
        /// <param name="path">Path to resource.</param>
        /// <param name="value">The JSON to PUT.</param>
        /// <returns>The PUT's response from the URI formed from the <paramref name="baseAddress"/> and <paramref name="path"/>.</returns>
        public async Task<string> PutContentAsync(string baseAddress, string path, object value)
        {
            var parameters = new NameValueCollection();
            return await PutContentAsync(baseAddress, path, parameters, value).ConfigureAwait(false);
        }

        /// <summary>
        /// Asynchronously PUTs the specified JSON <paramref name="value"/> to the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and single parameter, returning all the content of the response as a string.
        /// </summary>
        /// <param name="baseAddress">Base address.</param>
        /// <param name="path">Path to resource.</param>
        /// <param name="parameterName">Name of the parameter.</param>
        /// <param name="parameterValue">Value of the parameter.</param>
        /// <param name="value">The JSON to PUT.</param>
        /// <returns>The PUT's response from the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and single parameter.</returns>
        public async Task<string> PutContentAsync(string baseAddress, string path, string parameterName, string parameterValue, object value)
        {
            var parameters = new NameValueCollection
            {
                { parameterName, parameterValue }
            };
            return await PutContentAsync(baseAddress, path, parameters, value).ConfigureAwait(false);
        }

        /// <summary>
        /// Asynchronously PUTs the specified JSON <paramref name="value"/> to the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and two parameters, returning all the content of the response as a string.
        /// </summary>
        /// <param name="baseAddress">Base address.</param>
        /// <param name="path">Path to resource.</param>
        /// <param name="parameterName1">Name of the first parameter.</param>
        /// <param name="parameterValue1">Value of the first parameter.</param>
        /// <param name="parameterName2">Name of the second parameter.</param>
        /// <param name="parameterValue2">Value of the second parameter.</param>
        /// <param name="value">The JSON to PUT.</param>
        /// <returns>The PUT's response from the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and two parameters.</returns>
        public async Task<string> PutContentAsync(string baseAddress, string path, string parameterName1, string parameterValue1, string parameterName2, string parameterValue2, object value)
        {
            var parameters = new NameValueCollection
            {
                { parameterName1, parameterValue1 },
                { parameterName2, parameterValue2 }
            };
            return await PutContentAsync(baseAddress, path, parameters, value).ConfigureAwait(false);
        }

        /// <summary>
        /// Asynchronously PUTs the specified JSON <paramref name="value"/> to the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and three parameters, returning all the content of the response as a string.
        /// </summary>
        /// <param name="baseAddress">Base address.</param>
        /// <param name="path">Path to resource.</param>
        /// <param name="parameterName1">Name of the first parameter.</param>
        /// <param name="parameterValue1">Value of the first parameter.</param>
        /// <param name="parameterName2">Name of the second parameter.</param>
        /// <param name="parameterValue2">Value of the second parameter.</param>
        /// <param name="parameterName3">Name of the third parameter.</param>
        /// <param name="parameterValue3">Value of the third parameter.</param>
        /// <param name="value">The JSON to PUT.</param>
        /// <returns>The PUT's response from the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and three parameters.</returns>
        public async Task<string> PutContentAsync(string baseAddress, string path, string parameterName1, string parameterValue1, string parameterName2, string parameterValue2, string parameterName3, string parameterValue3, object value)
        {
            var parameters = new NameValueCollection
            {
                { parameterName1, parameterValue1 },
                { parameterName2, parameterValue2 },
                { parameterName3, parameterValue3 }
            };
            return await PutContentAsync(baseAddress, path, parameters, value).ConfigureAwait(false);
        }

        /// <summary>
        /// Asynchronously PUTs the specified JSON <paramref name="value"/> to the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and parameter collection, returning all the content of the response as a string.
        /// </summary>
        /// <param name="baseAddress">Base address.</param>
        /// <param name="path">Path to resource.</param>
        /// <param name="parameters">Collection of parameters.</param>
        /// <param name="value">The JSON to PUT.</param>
        /// <returns>The PUT's response from the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and parameter collection.</returns>
        public async Task<string> PutContentAsync(string baseAddress, string path, NameValueCollection parameters, object value) => await PutContentAsync(UriHelpers.BuildUri(baseAddress, path, parameters), value).ConfigureAwait(false);

        /// <summary>
        /// Asynchronously PUTs the specified JSON <paramref name="value"/> to the specified <paramref name="uri"/>, returning all the content of the response as a string.
        /// </summary>
        /// <param name="uri">Uri to the resource.</param>
        /// <param name="value">The JSON to PUT.</param>
        /// <returns>The PUT's response from the specified <paramref name="uri"/>.</returns>
        public async Task<string> PutContentAsync(Uri uri, object value) => await PerformRestContentOperationAsync(uri, RestOperation.Put, RestOperationContentType.Json, value).ConfigureAwait(false);
    }
}
