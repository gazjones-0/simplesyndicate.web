﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System;
using System.Collections.Specialized;
using System.Net.Http;

namespace SimpleSyndicate.Web
{
    /// <content>
    /// Contains PUT-related functionality.
    /// </content>
    public partial class RestClientBase
    {
        /// <overloads>
        /// <summary>
        /// Synchronously PUTs.
        /// </summary>
        /// </overloads>
        /// <summary>
        /// Synchronously PUTs the specified JSON <paramref name="value"/> to the specified <paramref name="address"/>.
        /// </summary>
        /// <param name="address">Uri to the resource.</param>
        /// <param name="value">The JSON to PUT.</param>
        /// <returns>The PUT's response.</returns>
        public HttpResponseMessage Put(string address, object value) => Put(new Uri(address), value);

        /// <summary>
        /// Synchronously PUTs the specified JSON <paramref name="value"/> to the URI formed from the <paramref name="baseAddress"/> and <paramref name="path"/>.
        /// </summary>
        /// <param name="baseAddress">Base address.</param>
        /// <param name="path">Path to resource.</param>
        /// <param name="value">The JSON to PUT.</param>
        /// <returns>The PUT's response from the URI formed from the <paramref name="baseAddress"/> and <paramref name="path"/>.</returns>
        public HttpResponseMessage Put(string baseAddress, string path, object value)
        {
            var parameters = new NameValueCollection();
            return Put(baseAddress, path, parameters, value);
        }

        /// <summary>
        /// Synchronously PUTs the specified JSON <paramref name="value"/> to the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and single parameter.
        /// </summary>
        /// <param name="baseAddress">Base address.</param>
        /// <param name="path">Path to resource.</param>
        /// <param name="parameterName">Name of the parameter.</param>
        /// <param name="parameterValue">Value of the parameter.</param>
        /// <param name="value">The JSON to PUT.</param>
        /// <returns>The PUT's response from the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and single parameter.</returns>
        public HttpResponseMessage Put(string baseAddress, string path, string parameterName, string parameterValue, object value)
        {
            var parameters = new NameValueCollection
            {
                { parameterName, parameterValue }
            };
            return Put(baseAddress, path, parameters, value);
        }

        /// <summary>
        /// Synchronously PUTs the specified JSON <paramref name="value"/> to the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and two parameters.
        /// </summary>
        /// <param name="baseAddress">Base address.</param>
        /// <param name="path">Path to resource.</param>
        /// <param name="parameterName1">Name of the first parameter.</param>
        /// <param name="parameterValue1">Value of the first parameter.</param>
        /// <param name="parameterName2">Name of the second parameter.</param>
        /// <param name="parameterValue2">Value of the second parameter.</param>
        /// <param name="value">The JSON to PUT.</param>
        /// <returns>The PUT's response from the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and two parameters.</returns>
        public HttpResponseMessage Put(string baseAddress, string path, string parameterName1, string parameterValue1, string parameterName2, string parameterValue2, object value)
        {
            var parameters = new NameValueCollection
            {
                { parameterName1, parameterValue1 },
                { parameterName2, parameterValue2 }
            };
            return Put(baseAddress, path, parameters, value);
        }

        /// <summary>
        /// Synchronously PUTs the specified JSON <paramref name="value"/> to the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and three parameters.
        /// </summary>
        /// <param name="baseAddress">Base address.</param>
        /// <param name="path">Path to resource.</param>
        /// <param name="parameterName1">Name of the first parameter.</param>
        /// <param name="parameterValue1">Value of the first parameter.</param>
        /// <param name="parameterName2">Name of the second parameter.</param>
        /// <param name="parameterValue2">Value of the second parameter.</param>
        /// <param name="parameterName3">Name of the third parameter.</param>
        /// <param name="parameterValue3">Value of the third parameter.</param>
        /// <param name="value">The JSON to PUT.</param>
        /// <returns>The PUT's response from the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and three parameters.</returns>
        public HttpResponseMessage Put(string baseAddress, string path, string parameterName1, string parameterValue1, string parameterName2, string parameterValue2, string parameterName3, string parameterValue3, object value)
        {
            var parameters = new NameValueCollection
            {
                { parameterName1, parameterValue1 },
                { parameterName2, parameterValue2 },
                { parameterName3, parameterValue3 }
            };
            return Put(baseAddress, path, parameters, value);
        }

        /// <summary>
        /// Synchronously PUTs the specified JSON <paramref name="value"/> to the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and parameter collection.
        /// </summary>
        /// <param name="baseAddress">Base address.</param>
        /// <param name="path">Path to resource.</param>
        /// <param name="parameters">Collection of parameters.</param>
        /// <param name="value">The JSON to PUT.</param>
        /// <returns>The PUT's response from the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and parameter collection.</returns>
        public HttpResponseMessage Put(string baseAddress, string path, NameValueCollection parameters, object value)
        {
            var response = PutAsync(baseAddress, path, parameters, value);
            response.Wait();
            return response.Result;
        }

        /// <summary>
        /// Synchronously PUTs the specified JSON <paramref name="value"/> to the specified <paramref name="uri"/>.
        /// </summary>
        /// <param name="uri">Uri to the resource.</param>
        /// <param name="value">The JSON to PUT.</param>
        /// <returns>The PUT's response from the specified <paramref name="uri"/>.</returns>
        public HttpResponseMessage Put(Uri uri, object value)
        {
            var response = PutAsync(uri, value);
            response.Wait();
            return response.Result;
        }
    }
}
