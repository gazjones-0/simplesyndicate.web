﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace SimpleSyndicate.Web
{
    /// <summary>
    /// <para>
    /// JSON client used for interacting with REST resources that take and return JSON.
    /// </para>
    /// <para>
    /// <code language="cs" source="..\..\src\SimpleSyndicate.Web\SimpleSyndicate.Web.Examples\Json.cs" />
    /// </para>
    /// </summary>
    /// <example>
    /// <code language="cs" source="..\..\src\SimpleSyndicate.Web\SimpleSyndicate.Web.Examples\Json.cs" />
    /// </example>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1506", Justification = "Large number of types dictated by design.")]
    public partial class JsonClient
    {
        /// <summary>
        /// REST client used to perform the actual interaction with REST resources.
        /// </summary>
        private RestClient _restClient;

        /// <summary>
        /// Initializes a new instance of the <see cref="JsonClient"/> class using the specified <paramref name="responseHandler"/> and <paramref name="httpClientFactory"/>.
        /// </summary>
        /// <param name="responseHandler">An <see cref="IRestResponseHandler"/> that is used to check if a REST request was successful or not.</param>
        /// <param name="httpClientFactory">An <see cref="IHttpClientFactory"/> that is used to create HTTP clients that are used to make REST requests.</param>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="responseHandler"/> or <paramref name="httpClientFactory"/> is <c>null</c>.</exception>
        public JsonClient(IRestResponseHandler responseHandler, IHttpClientFactory httpClientFactory)
        {
            if (responseHandler == null)
            {
                throw new ArgumentNullException(nameof(responseHandler));
            }

            if (httpClientFactory == null)
            {
                throw new ArgumentNullException(nameof(httpClientFactory));
            }

            _restClient = new RestClient(responseHandler, httpClientFactory);
        }

        /// <summary>
        /// REST operation being performed.
        /// </summary>
        private enum RestOperation
        {
            /// <summary>
            /// GET operation.
            /// </summary>
            Get,

            /// <summary>
            /// POST operation.
            /// </summary>
            Post,

            /// <summary>
            /// PUT operation.
            /// </summary>
            Put
        }

        /// <summary>
        /// Gets or sets the response handler used to check if the REST response was successful or not.
        /// </summary>
        /// <value>An <see cref="IRestResponseHandler"/>.</value>
        public IRestResponseHandler ResponseHandler
        {
            get => _restClient.ResponseHandler;

            set => _restClient.ResponseHandler = value;
        }

        /// <summary>
        /// Gets or sets the HTTP client factory used to create HTTP clients that are used to make REST requests.
        /// </summary>
        /// <value>An <see cref="IHttpClientFactory"/>.</value>
        public IHttpClientFactory HttpClientFactory
        {
            get => _restClient.HttpClientFactory;

            set => _restClient.HttpClientFactory = value;
        }

        /// <summary>
        /// Performs a JSON operation via REST.
        /// </summary>
        /// <typeparam name="TJson">The type to deserialize the JSON to.</typeparam>
        /// <param name="uri">Uri to the resource.</param>
        /// <param name="operation">Operation to perform.</param>
        /// <param name="value">Optional value for the operation.</param>
        /// <returns>The deserialized response.</returns>
        private async Task<TJson> PerformJsonOperationAsync<TJson>(Uri uri, RestOperation operation, object value)
        {
            string content = null;
            switch (operation)
            {
                case RestOperation.Get:
                    content = await _restClient.GetContentAsync(uri).ConfigureAwait(false);
                    break;

                case RestOperation.Post:
                    content = await _restClient.PostContentAsync(uri, value).ConfigureAwait(false);
                    break;

                case RestOperation.Put:
                    content = await _restClient.PutContentAsync(uri, value).ConfigureAwait(false);
                    break;
            }

            // we can't deserialize to a string, so if we've been asked one, assume they just want the raw contents
            if (typeof(TJson) == typeof(string))
            {
                return (TJson)((object)content);
            }

            return await Task.Run(() => JsonConvert.DeserializeObject<TJson>(content)).ConfigureAwait(false);
        }

        /// <summary>
        /// Performs a paged JSON operation via REST.
        /// </summary>
        /// <typeparam name="TPage">The type to deserialize pages to.</typeparam>
        /// <typeparam name="TPageItem">The type to deserialize page items to.</typeparam>
        /// <param name="uri">Uri to the resource.</param>
        /// <param name="nextPageUrl">Expression that determines how to get the next page url.</param>
        /// <param name="items">Expression that determines how to get the items in a page.</param>
        /// <returns>The combined set of pages as a <typeparamref name="TPage"/>.</returns>
        private async Task<TPage> PerformPagedJsonOperationAsync<TPage, TPageItem>(Uri uri, Expression<Func<TPage, string>> nextPageUrl, Expression<Func<TPage, IList<TPageItem>>> items) => await PerformPagedJsonOperationAsync<TPage, TPageItem>(uri, nextPageUrl, items, null).ConfigureAwait(false);

        /// <summary>
        /// Performs a paged JSON operation via REST, performing a specified <paramref name="itemAction"/> on each page item.
        /// </summary>
        /// <typeparam name="TPage">The type to deserialize pages to.</typeparam>
        /// <typeparam name="TPageItem">The type to deserialize page items to.</typeparam>
        /// <param name="uri">Uri to the resource.</param>
        /// <param name="nextPageUrl">Expression that determines how to get the next page url.</param>
        /// <param name="items">Expression that determines how to get the items in a page.</param>
        /// <param name="itemAction">Action to perform on each page item.</param>
        /// <returns>The combined set of pages as a <typeparamref name="TPage"/>.</returns>
        private async Task<TPage> PerformPagedJsonOperationAsync<TPage, TPageItem>(Uri uri, Expression<Func<TPage, string>> nextPageUrl, Expression<Func<TPage, IList<TPageItem>>> items, Action<TPageItem> itemAction)
        {
            var url = uri.ToString();

            // compiled expressions that return the next page and the items on a page
            var compiledNextPageUrl = nextPageUrl.Compile();
            var compiledItems = items.Compile();
            var combinedPages = default(TPage);
            while (!string.IsNullOrWhiteSpace(url))
            {
                var page = await GetAsync<TPage>(url).ConfigureAwait(false);

                // if this is the first page retrieved, make the combined set of pages this page
                if (combinedPages == null)
                {
                    combinedPages = page;
                }
                else
                {
                    // not the first page, so add the items from the retrieved page to our combined set
                    var pageItems = compiledItems(page);
                    var combinedPagesItems = compiledItems(combinedPages);

                    // take advantage of List.AddRange if the items are a List
                    if (combinedPagesItems is List<TPageItem>)
                    {
                        ((List<TPageItem>)combinedPagesItems).AddRange(pageItems);
                    }
                    else
                    {
                        foreach (var item in combinedPagesItems)
                        {
                            combinedPagesItems.Add(item);
                        }
                    }
                }

                url = compiledNextPageUrl(page);
            }

            // if there's an action specified, apply it to each item
            if (itemAction != null)
            {
                foreach (var item in compiledItems(combinedPages))
                {
                    itemAction(item);
                }
            }

            return combinedPages;
        }
    }
}
