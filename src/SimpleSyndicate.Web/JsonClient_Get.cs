﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System;
using System.Collections.Specialized;

namespace SimpleSyndicate.Web
{
    /// <content>
    /// Contains GET-related functionality.
    /// </content>
    public partial class JsonClient
    {
        /// <overloads>
        /// <summary>
        /// Synchronously GETs JSON, deserializing the response.
        /// </summary>
        /// </overloads>
        /// <summary>
        /// Makes a synchronous GET request to the specified <paramref name="address"/>, de-serializing the response to a dynamic object.
        /// </summary>
        /// <param name="address">Path to the resource.</param>
        /// <returns>The deserialized GET's JSON response.</returns>
        public dynamic Get(string address)
        {
            var response = GetAsync<dynamic>(new Uri(address));
            response.Wait();
            return response.Result;
        }

        /// <summary>
        /// Makes a synchronous GET request to the URI formed from the <paramref name="baseAddress"/> and the <paramref name="path"/>, de-serializing the response to a dynamic object.
        /// </summary>
        /// <param name="baseAddress">Base address.</param>
        /// <param name="path">Path to resource.</param>
        /// <returns>The deserialized GET's JSON response from the URI formed from the <paramref name="baseAddress"/> and the <paramref name="path"/>.</returns>
        public dynamic Get(string baseAddress, string path)
        {
            var response = GetAsync<dynamic>(baseAddress, path);
            response.Wait();
            return response.Result;
        }

        /// <summary>
        /// Makes a synchronous GET request to the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and single parameter, de-serializing the response to a dynamic object.
        /// </summary>
        /// <param name="baseAddress">Base address.</param>
        /// <param name="path">Path to resource.</param>
        /// <param name="parameterName">Name of the parameter.</param>
        /// <param name="parameterValue">Value of the parameter.</param>
        /// <returns>The deserialized GET's JSON response from the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and single parameter.</returns>
        public dynamic Get(string baseAddress, string path, string parameterName, string parameterValue)
        {
            var response = GetAsync<dynamic>(baseAddress, path, parameterName, parameterValue);
            response.Wait();
            return response.Result;
        }

        /// <summary>
        /// Makes a synchronous GET request to the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and two parameters, de-serializing the response to a dynamic object.
        /// </summary>
        /// <param name="baseAddress">Base address.</param>
        /// <param name="path">Path to resource.</param>
        /// <param name="parameterName1">Name of the first parameter.</param>
        /// <param name="parameterValue1">Value of the first parameter.</param>
        /// <param name="parameterName2">Name of the second parameter.</param>
        /// <param name="parameterValue2">Value of the second parameter.</param>
        /// <returns>The deserialized GET's JSON response from the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and two parameters.</returns>
        public dynamic Get(string baseAddress, string path, string parameterName1, string parameterValue1, string parameterName2, string parameterValue2)
        {
            var response = GetAsync<dynamic>(baseAddress, path, parameterName1, parameterValue1, parameterName2, parameterValue2);
            response.Wait();
            return response.Result;
        }

        /// <summary>
        /// Makes a synchronous GET request to the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and three parameters, de-serializing the response to a dynamic object.
        /// </summary>
        /// <param name="baseAddress">Base address.</param>
        /// <param name="path">Path to resource.</param>
        /// <param name="parameterName1">Name of the first parameter.</param>
        /// <param name="parameterValue1">Value of the first parameter.</param>
        /// <param name="parameterName2">Name of the second parameter.</param>
        /// <param name="parameterValue2">Value of the second parameter.</param>
        /// <param name="parameterName3">Name of the third parameter.</param>
        /// <param name="parameterValue3">Value of the third parameter.</param>
        /// <returns>The deserialized GET's JSON response from the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and three parameters.</returns>
        public dynamic Get(string baseAddress, string path, string parameterName1, string parameterValue1, string parameterName2, string parameterValue2, string parameterName3, string parameterValue3)
        {
            var response = GetAsync<dynamic>(baseAddress, path, parameterName1, parameterValue1, parameterName2, parameterValue2, parameterName3, parameterValue3);
            response.Wait();
            return response.Result;
        }

        /// <summary>
        /// Makes a synchronous GET request to the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and parameter collection, de-serializing the response to a dynamic object.
        /// </summary>
        /// <param name="baseAddress">Base address.</param>
        /// <param name="path">Path to resource.</param>
        /// <param name="parameters">Collection of parameters.</param>
        /// <returns>The deserialized GET's JSON response from the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and parameter collection.</returns>
        public dynamic Get(string baseAddress, string path, NameValueCollection parameters)
        {
            var response = GetAsync<dynamic>(baseAddress, path, parameters);
            response.Wait();
            return response.Result;
        }

        /// <summary>
        /// Makes a synchronous GET request to the specified <paramref name="uri"/>, de-serializing the response to a dynamic object.
        /// </summary>
        /// <param name="uri">Uri to the resource.</param>
        /// <returns>The deserialized GET's JSON response from the specified <paramref name="uri"/>.</returns>
        public dynamic Get(Uri uri)
        {
            var response = GetAsync<dynamic>(uri);
            response.Wait();
            return response.Result;
        }

        /// <summary>
        /// Makes a synchronous GET request to the specified <paramref name="address"/>, de-serializing the response to <typeparamref name="TJson"/>.
        /// </summary>
        /// <typeparam name="TJson">The type to deserialize the JSON to.</typeparam>
        /// <param name="address">Path to the resource.</param>
        /// <returns>The deserialized GET's JSON response.</returns>
        public TJson Get<TJson>(string address)
        {
            var response = GetAsync<TJson>(new Uri(address));
            response.Wait();
            return response.Result;
        }

        /// <summary>
        /// Makes a synchronous GET request to the URI formed from the <paramref name="baseAddress"/> and the <paramref name="path"/>, de-serializing the response to <typeparamref name="TJson"/>.
        /// </summary>
        /// <typeparam name="TJson">The type to deserialize the JSON to.</typeparam>
        /// <param name="baseAddress">Base address.</param>
        /// <param name="path">Path to resource.</param>
        /// <returns>The deserialized GET's JSON response from the URI formed from the <paramref name="baseAddress"/> and the <paramref name="path"/>.</returns>
        public TJson Get<TJson>(string baseAddress, string path)
        {
            var response = GetAsync<TJson>(baseAddress, path);
            response.Wait();
            return response.Result;
        }

        /// <summary>
        /// Makes a synchronous GET request to the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and single parameter, de-serializing the response to <typeparamref name="TJson"/>.
        /// </summary>
        /// <typeparam name="TJson">The type to deserialize the JSON to.</typeparam>
        /// <param name="baseAddress">Base address.</param>
        /// <param name="path">Path to resource.</param>
        /// <param name="parameterName">Name of the parameter.</param>
        /// <param name="parameterValue">Value of the parameter.</param>
        /// <returns>The deserialized GET's JSON response from the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and single parameter.</returns>
        public TJson Get<TJson>(string baseAddress, string path, string parameterName, string parameterValue)
        {
            var response = GetAsync<TJson>(baseAddress, path, parameterName, parameterValue);
            response.Wait();
            return response.Result;
        }

        /// <summary>
        /// Makes a synchronous GET request to the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and two parameters, de-serializing the response to <typeparamref name="TJson"/>.
        /// </summary>
        /// <typeparam name="TJson">The type to deserialize the JSON to.</typeparam>
        /// <param name="baseAddress">Base address.</param>
        /// <param name="path">Path to resource.</param>
        /// <param name="parameterName1">Name of the first parameter.</param>
        /// <param name="parameterValue1">Value of the first parameter.</param>
        /// <param name="parameterName2">Name of the second parameter.</param>
        /// <param name="parameterValue2">Value of the second parameter.</param>
        /// <returns>The deserialized GET's JSON response from the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and two parameters.</returns>
        public TJson Get<TJson>(string baseAddress, string path, string parameterName1, string parameterValue1, string parameterName2, string parameterValue2)
        {
            var response = GetAsync<TJson>(baseAddress, path, parameterName1, parameterValue1, parameterName2, parameterValue2);
            response.Wait();
            return response.Result;
        }

        /// <summary>
        /// Makes a synchronous GET request to the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and three parameters, de-serializing the response to <typeparamref name="TJson"/>.
        /// </summary>
        /// <typeparam name="TJson">The type to deserialize the JSON to.</typeparam>
        /// <param name="baseAddress">Base address.</param>
        /// <param name="path">Path to resource.</param>
        /// <param name="parameterName1">Name of the first parameter.</param>
        /// <param name="parameterValue1">Value of the first parameter.</param>
        /// <param name="parameterName2">Name of the second parameter.</param>
        /// <param name="parameterValue2">Value of the second parameter.</param>
        /// <param name="parameterName3">Name of the third parameter.</param>
        /// <param name="parameterValue3">Value of the third parameter.</param>
        /// <returns>The deserialized GET's JSON response from the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and three parameters.</returns>
        public TJson Get<TJson>(string baseAddress, string path, string parameterName1, string parameterValue1, string parameterName2, string parameterValue2, string parameterName3, string parameterValue3)
        {
            var response = GetAsync<TJson>(baseAddress, path, parameterName1, parameterValue1, parameterName2, parameterValue2, parameterName3, parameterValue3);
            response.Wait();
            return response.Result;
        }

        /// <summary>
        /// Makes a synchronous GET request to the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and parameter collection, de-serializing the response to <typeparamref name="TJson"/>.
        /// </summary>
        /// <typeparam name="TJson">The type to deserialize the JSON to.</typeparam>
        /// <param name="baseAddress">Base address.</param>
        /// <param name="path">Path to resource.</param>
        /// <param name="parameters">Collection of parameters.</param>
        /// <returns>The deserialized GET's JSON response from the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and parameter collection.</returns>
        public TJson Get<TJson>(string baseAddress, string path, NameValueCollection parameters)
        {
            var response = GetAsync<TJson>(baseAddress, path, parameters);
            response.Wait();
            return response.Result;
        }

        /// <summary>
        /// Makes a synchronous GET request to the specified <paramref name="uri"/>, de-serializing the response to <typeparamref name="TJson"/>.
        /// </summary>
        /// <typeparam name="TJson">The type to deserialize the JSON to.</typeparam>
        /// <param name="uri">Uri to the resource.</param>
        /// <returns>The deserialized GET's JSON response from the specified <paramref name="uri"/>.</returns>
        public TJson Get<TJson>(Uri uri)
        {
            var response = GetAsync<TJson>(uri);
            response.Wait();
            return response.Result;
        }
    }
}
