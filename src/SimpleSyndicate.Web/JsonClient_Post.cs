﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System;
using System.Collections.Specialized;

namespace SimpleSyndicate.Web
{
    /// <content>
    /// Contains POST-related functionality.
    /// </content>
    public partial class JsonClient
    {
        /// <overloads>
        /// <summary>
        /// Synchronously POSTs JSON, deserializing the response.
        /// </summary>
        /// </overloads>
        /// <summary>
        /// Synchronously POSTs the specified JSON <paramref name="value"/> to the specified <paramref name="address"/>, de-serializing the response to a dynamic object.
        /// </summary>
        /// <param name="address">Uri to the resource.</param>
        /// <param name="value">The JSON to POST.</param>
        /// <returns>The deserialized POST's JSON response.</returns>
        public dynamic Post(string address, object value)
        {
            var response = PostAsync<dynamic>(new Uri(address), value);
            response.Wait();
            return response.Result;
        }

        /// <summary>
        /// Synchronously POSTs the specified JSON <paramref name="value"/> to the URI formed from the <paramref name="baseAddress"/> and <paramref name="path"/>, de-serializing the response to a dynamic object.
        /// </summary>
        /// <param name="baseAddress">Base address.</param>
        /// <param name="path">Path to resource.</param>
        /// <param name="value">The JSON to POST.</param>
        /// <returns>The deserialized POST's JSON response from the URI formed from the <paramref name="baseAddress"/> and <paramref name="path"/>.</returns>
        public dynamic Post(string baseAddress, string path, object value)
        {
            var response = PostAsync<dynamic>(baseAddress, path, value);
            response.Wait();
            return response.Result;
        }

        /// <summary>
        /// Synchronously POSTs the specified JSON <paramref name="value"/> to the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and single parameter, de-serializing the response to a dynamic object.
        /// </summary>
        /// <param name="baseAddress">Base address.</param>
        /// <param name="path">Path to resource.</param>
        /// <param name="parameterName">Name of the parameter.</param>
        /// <param name="parameterValue">Value of the parameter.</param>
        /// <param name="value">The JSON to POST.</param>
        /// <returns>The deserialized POST's JSON response from the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and single parameter.</returns>
        public dynamic Post(string baseAddress, string path, string parameterName, string parameterValue, object value)
        {
            var response = PostAsync<dynamic>(baseAddress, path, parameterName, parameterValue, value);
            response.Wait();
            return response.Result;
        }

        /// <summary>
        /// Synchronously POSTs the specified JSON <paramref name="value"/> to the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and two parameters, de-serializing the response to a dynamic object.
        /// </summary>
        /// <param name="baseAddress">Base address.</param>
        /// <param name="path">Path to resource.</param>
        /// <param name="parameterName1">Name of the first parameter.</param>
        /// <param name="parameterValue1">Value of the first parameter.</param>
        /// <param name="parameterName2">Name of the second parameter.</param>
        /// <param name="parameterValue2">Value of the second parameter.</param>
        /// <param name="value">The JSON to POST.</param>
        /// <returns>The deserialized POST's JSON response from the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and two parameters.</returns>
        public dynamic Post(string baseAddress, string path, string parameterName1, string parameterValue1, string parameterName2, string parameterValue2, object value)
        {
            var response = PostAsync<dynamic>(baseAddress, path, parameterName1, parameterValue1, parameterName2, parameterValue2, value);
            response.Wait();
            return response.Result;
        }

        /// <summary>
        /// Synchronously POSTs the specified JSON <paramref name="value"/> to the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and three parameters, de-serializing the response to a dynamic object.
        /// </summary>
        /// <param name="baseAddress">Base address.</param>
        /// <param name="path">Path to resource.</param>
        /// <param name="parameterName1">Name of the first parameter.</param>
        /// <param name="parameterValue1">Value of the first parameter.</param>
        /// <param name="parameterName2">Name of the second parameter.</param>
        /// <param name="parameterValue2">Value of the second parameter.</param>
        /// <param name="parameterName3">Name of the third parameter.</param>
        /// <param name="parameterValue3">Value of the third parameter.</param>
        /// <param name="value">The JSON to POST.</param>
        /// <returns>The deserialized POST's JSON response from the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and three parameters.</returns>
        public dynamic Post(string baseAddress, string path, string parameterName1, string parameterValue1, string parameterName2, string parameterValue2, string parameterName3, string parameterValue3, object value)
        {
            var response = PostAsync<dynamic>(baseAddress, path, parameterName1, parameterValue1, parameterName2, parameterValue2, parameterName3, parameterValue3, value);
            response.Wait();
            return response.Result;
        }

        /// <summary>
        /// Synchronously POSTs the specified JSON <paramref name="value"/> to the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and parameter collection, de-serializing the response to a dynamic object.
        /// </summary>
        /// <param name="baseAddress">Base address.</param>
        /// <param name="path">Path to resource.</param>
        /// <param name="parameters">Collection of parameters.</param>
        /// <param name="value">The JSON to POST.</param>
        /// <returns>The deserialized POST's JSON response from the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and parameter collection.</returns>
        public dynamic Post(string baseAddress, string path, NameValueCollection parameters, object value)
        {
            var response = PostAsync<dynamic>(baseAddress, path, parameters, value);
            response.Wait();
            return response.Result;
        }

        /// <summary>
        /// Synchronously POSTs the specified JSON <paramref name="value"/> to the specified <paramref name="uri"/>, de-serializing the response to a dynamic object.
        /// </summary>
        /// <param name="uri">Uri to the resource.</param>
        /// <param name="value">The JSON to POST.</param>
        /// <returns>The deserialized POST's JSON response from the specified <paramref name="uri"/>.</returns>
        public dynamic Post(Uri uri, object value)
        {
            var response = PostAsync<dynamic>(uri, value);
            response.Wait();
            return response.Result;
        }

        /// <summary>
        /// Synchronously POSTs the specified JSON <paramref name="value"/> to the specified <paramref name="address"/>, de-serializing the response to <typeparamref name="TJson"/>.
        /// </summary>
        /// <typeparam name="TJson">The type to deserialize the JSON to.</typeparam>
        /// <param name="address">Uri to the resource.</param>
        /// <param name="value">The JSON to POST.</param>
        /// <returns>The deserialized POST's JSON response.</returns>
        public TJson Post<TJson>(string address, object value)
        {
            var response = PostAsync<TJson>(new Uri(address), value);
            response.Wait();
            return response.Result;
        }

        /// <summary>
        /// Synchronously POSTs the specified JSON <paramref name="value"/> to the URI formed from the <paramref name="baseAddress"/> and <paramref name="path"/>, de-serializing the response to <typeparamref name="TJson"/>.
        /// </summary>
        /// <typeparam name="TJson">The type to deserialize the JSON to.</typeparam>
        /// <param name="baseAddress">Base address.</param>
        /// <param name="path">Path to resource.</param>
        /// <param name="value">The JSON to POST.</param>
        /// <returns>The deserialized POST's JSON response from the URI formed from the <paramref name="baseAddress"/> and <paramref name="path"/>.</returns>
        public TJson Post<TJson>(string baseAddress, string path, object value)
        {
            var response = PostAsync<TJson>(baseAddress, path, value);
            response.Wait();
            return response.Result;
        }

        /// <summary>
        /// Synchronously POSTs the specified JSON <paramref name="value"/> to the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and single parameter, de-serializing the response to <typeparamref name="TJson"/>.
        /// </summary>
        /// <typeparam name="TJson">The type to deserialize the JSON to.</typeparam>
        /// <param name="baseAddress">Base address.</param>
        /// <param name="path">Path to resource.</param>
        /// <param name="parameterName">Name of the parameter.</param>
        /// <param name="parameterValue">Value of the parameter.</param>
        /// <param name="value">The JSON to POST.</param>
        /// <returns>The deserialized POST's JSON response from the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and single parameter.</returns>
        public TJson Post<TJson>(string baseAddress, string path, string parameterName, string parameterValue, object value)
        {
            var response = PostAsync<TJson>(baseAddress, path, parameterName, parameterValue, value);
            response.Wait();
            return response.Result;
        }

        /// <summary>
        /// Synchronously POSTs the specified JSON <paramref name="value"/> to the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and two parameters, de-serializing the response to <typeparamref name="TJson"/>.
        /// </summary>
        /// <typeparam name="TJson">The type to deserialize the JSON to.</typeparam>
        /// <param name="baseAddress">Base address.</param>
        /// <param name="path">Path to resource.</param>
        /// <param name="parameterName1">Name of the first parameter.</param>
        /// <param name="parameterValue1">Value of the first parameter.</param>
        /// <param name="parameterName2">Name of the second parameter.</param>
        /// <param name="parameterValue2">Value of the second parameter.</param>
        /// <param name="value">The JSON to POST.</param>
        /// <returns>The deserialized POST's JSON response from the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and two parameters.</returns>
        public TJson Post<TJson>(string baseAddress, string path, string parameterName1, string parameterValue1, string parameterName2, string parameterValue2, object value)
        {
            var response = PostAsync<TJson>(baseAddress, path, parameterName1, parameterValue1, parameterName2, parameterValue2, value);
            response.Wait();
            return response.Result;
        }

        /// <summary>
        /// Synchronously POSTs the specified JSON <paramref name="value"/> to the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and three parameters, de-serializing the response to <typeparamref name="TJson"/>.
        /// </summary>
        /// <typeparam name="TJson">The type to deserialize the JSON to.</typeparam>
        /// <param name="baseAddress">Base address.</param>
        /// <param name="path">Path to resource.</param>
        /// <param name="parameterName1">Name of the first parameter.</param>
        /// <param name="parameterValue1">Value of the first parameter.</param>
        /// <param name="parameterName2">Name of the second parameter.</param>
        /// <param name="parameterValue2">Value of the second parameter.</param>
        /// <param name="parameterName3">Name of the third parameter.</param>
        /// <param name="parameterValue3">Value of the third parameter.</param>
        /// <param name="value">The JSON to POST.</param>
        /// <returns>The deserialized POST's JSON response from the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and three parameters.</returns>
        public TJson Post<TJson>(string baseAddress, string path, string parameterName1, string parameterValue1, string parameterName2, string parameterValue2, string parameterName3, string parameterValue3, object value)
        {
            var response = PostAsync<TJson>(baseAddress, path, parameterName1, parameterValue1, parameterName2, parameterValue2, parameterName3, parameterValue3, value);
            response.Wait();
            return response.Result;
        }

        /// <summary>
        /// Synchronously POSTs the specified JSON <paramref name="value"/> to the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and parameter collection, de-serializing the response to <typeparamref name="TJson"/>.
        /// </summary>
        /// <typeparam name="TJson">The type to deserialize the JSON to.</typeparam>
        /// <param name="baseAddress">Base address.</param>
        /// <param name="path">Path to resource.</param>
        /// <param name="parameters">Collection of parameters.</param>
        /// <param name="value">The JSON to POST.</param>
        /// <returns>The deserialized POST's JSON response from the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and parameter collection.</returns>
        public TJson Post<TJson>(string baseAddress, string path, NameValueCollection parameters, object value)
        {
            var response = PostAsync<TJson>(baseAddress, path, parameters, value);
            response.Wait();
            return response.Result;
        }

        /// <summary>
        /// Synchronously POSTs the specified JSON <paramref name="value"/> to the specified <paramref name="uri"/>, de-serializing the response to <typeparamref name="TJson"/>.
        /// </summary>
        /// <typeparam name="TJson">The type to deserialize the JSON to.</typeparam>
        /// <param name="uri">Uri to the resource.</param>
        /// <param name="value">The JSON to POST.</param>
        /// <returns>The deserialized POST's JSON response from the specified <paramref name="uri"/>.</returns>
        public TJson Post<TJson>(Uri uri, object value)
        {
            var response = PostAsync<TJson>(uri, value);
            response.Wait();
            return response.Result;
        }
    }
}
