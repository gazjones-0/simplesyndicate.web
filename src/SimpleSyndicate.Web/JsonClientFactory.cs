﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System;
using System.Net.Http.Headers;
using SimpleSyndicate.Web.HttpClientFactories;
using SimpleSyndicate.Web.RestResponseHandlers;

namespace SimpleSyndicate.Web
{
    /// <summary>
    /// Factory class for creating <see cref="JsonClient"/>s.
    /// </summary>
    public static class JsonClientFactory
    {
        /// <overloads>
        /// Creates a new instance of the <see cref="JsonClient"/> class.
        /// </overloads>
        /// <summary>
        /// Creates a new instance of the <see cref="JsonClient"/> class; the <see cref="StandardRestResponseHandler"/> will be used to check if
        /// REST requests are successful or not, and the <see cref="StandardHttpClientFactory"/> will be used to create HTTP clients that are used to make
        /// REST requests.
        /// </summary>
        /// <returns>A <see cref="JsonClient"/> instance.</returns>
        public static JsonClient Create()
        {
            return new JsonClient(new StandardRestResponseHandler(), new StandardHttpClientFactory());
        }

        /// <summary>
        /// Creates a new instance of the <see cref="JsonClient"/> class; the <see cref="StandardRestResponseHandler"/> will be used to check if
        /// REST requests are successful or not, and the <see cref="StandardHttpClientFactory"/> will be used to create HTTP clients that are used to make
        /// REST requests, using the specified <paramref name="authenticationHeaderValue"/>.
        /// </summary>
        /// <param name="authenticationHeaderValue">The authentication information.</param>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="authenticationHeaderValue"/> is <c>null</c>.</exception>
        /// <returns>A <see cref="JsonClient"/> instance.</returns>
        public static JsonClient Create(AuthenticationHeaderValue authenticationHeaderValue)
        {
            if (authenticationHeaderValue == null)
            {
                throw new ArgumentNullException(nameof(authenticationHeaderValue));
            }

            return new JsonClient(new StandardRestResponseHandler(), new StandardHttpClientFactory(authenticationHeaderValue));
        }

        /// <summary>
        /// Creates a new instance of the <see cref="JsonClient"/> class for use with services secured by <c>OAuth2</c> client credentials; the
        /// <see cref="StandardHttpClientFactory"/> will be used to create HTTP clients that are used to make REST requests, and the
        /// <see cref="OAuth2ClientCredentialsRestResponseHandler"/> will handle the response and authentication.
        /// </summary>
        /// <param name="tokenEndpoint">Token endpoint.</param>
        /// <param name="username">Client username.</param>
        /// <param name="password">Client password.</param>
        /// <param name="clientId">Client id.</param>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="tokenEndpoint"/> is <c>null</c>.</exception>
        /// <returns>A <see cref="JsonClient"/> instance.</returns>
        public static JsonClient CreateUsingJwt(string tokenEndpoint, string username, string password, string clientId)
        {
            if (tokenEndpoint == null)
            {
                throw new ArgumentNullException(nameof(tokenEndpoint));
            }

            return CreateUsingJwt(new Uri(tokenEndpoint), username, password, clientId);
        }

        /// <summary>
        /// Creates a new instance of the <see cref="JsonClient"/> class for use with services secured by <c>OAuth2</c> client credentials; the
        /// <see cref="StandardHttpClientFactory"/> will be used to create HTTP clients that are used to make REST requests, and the
        /// <see cref="OAuth2ClientCredentialsRestResponseHandler"/> will handle the response and authentication.
        /// </summary>
        /// <param name="tokenEndpoint">Token endpoint.</param>
        /// <param name="username">Client username.</param>
        /// <param name="password">Client password.</param>
        /// <param name="clientId">Client id.</param>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="tokenEndpoint"/> is <c>null</c>.</exception>
        /// <returns>A <see cref="JsonClient"/> instance.</returns>
        public static JsonClient CreateUsingJwt(Uri tokenEndpoint, string username, string password, string clientId)
        {
            var responseHandler = new JwtRestResponseHandler(tokenEndpoint, username, password, clientId);
            return new JsonClient(responseHandler, new StandardHttpClientFactory());
        }

        /// <summary>
        /// Creates a new instance of the <see cref="JsonClient"/> class for use with services secured by <c>OAuth2</c> client credentials; the
        /// <see cref="StandardHttpClientFactory"/> will be used to create HTTP clients that are used to make REST requests, and the
        /// <see cref="OAuth2ClientCredentialsRestResponseHandler"/> will handle the response and authentication.
        /// </summary>
        /// <param name="tokenEndpoint">Token endpoint.</param>
        /// <param name="clientId">Client id.</param>
        /// <param name="clientSecret">Client secret.</param>
        /// <param name="scope">Token scope.</param>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="tokenEndpoint"/> is <c>null</c>.</exception>
        /// <returns>A <see cref="JsonClient"/> instance.</returns>
        public static JsonClient CreateUsingOAuth2ClientCredentials(string tokenEndpoint, string clientId, string clientSecret, string scope)
        {
            if (tokenEndpoint == null)
            {
                throw new ArgumentNullException(nameof(tokenEndpoint));
            }

            return CreateUsingOAuth2ClientCredentials(new Uri(tokenEndpoint), clientId, clientSecret, scope);
        }

        /// <summary>
        /// Creates a new instance of the <see cref="JsonClient"/> class for use with services secured by <c>OAuth2</c> client credentials; the
        /// <see cref="StandardHttpClientFactory"/> will be used to create HTTP clients that are used to make REST requests, and the
        /// <see cref="OAuth2ClientCredentialsRestResponseHandler"/> will handle the response and authentication.
        /// </summary>
        /// <param name="tokenEndpoint">Token endpoint.</param>
        /// <param name="clientId">Client id.</param>
        /// <param name="clientSecret">Client secret.</param>
        /// <param name="scope">Token scope.</param>
        /// <returns>A <see cref="JsonClient"/> instance.</returns>
        public static JsonClient CreateUsingOAuth2ClientCredentials(Uri tokenEndpoint, string clientId, string clientSecret, string scope)
        {
            var responseHandler = new OAuth2ClientCredentialsRestResponseHandler(tokenEndpoint, clientId, clientSecret, scope);
            return new JsonClient(responseHandler, new StandardHttpClientFactory());
        }
    }
}
