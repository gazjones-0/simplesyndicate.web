﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using SimpleSyndicate.Web.HttpClientFactories;

namespace SimpleSyndicate.Web.RestResponseHandlers
{
    /// <summary>
    /// Authenticated response handler for <see cref="RestClient"/>s using <c>JWT</c> services; if the service responds with
    /// <see cref="HttpStatusCode.Unauthorized"/>, it will automatically re-authenticate and re-try the request.
    /// </summary>
    public class JwtRestResponseHandler : AuthenticatedRestResponseHandlerBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="JwtRestResponseHandler"/> class using the specified token retrieval details.
        /// </summary>
        /// <param name="tokenEndpoint">Token endpoint.</param>
        /// <param name="username">Client username.</param>
        /// <param name="password">Client password.</param>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="tokenEndpoint"/>, <paramref name="username"/> or <paramref name="password"/> are <c>null</c>.</exception>
        /// <exception cref="ArgumentException">Thrown when <paramref name="username"/> is blank.</exception>
        public JwtRestResponseHandler(string tokenEndpoint, string username, string password)
            : this(new Uri(tokenEndpoint), username, password, "password", null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="JwtRestResponseHandler"/> class using the specified token retrieval details.
        /// </summary>
        /// <param name="tokenEndpoint">Token endpoint.</param>
        /// <param name="username">Client username.</param>
        /// <param name="password">Client password.</param>
        /// <param name="clientId">Client id.</param>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="tokenEndpoint"/>, <paramref name="username"/>, <paramref name="password"/> or <paramref name="clientId"/> are <c>null</c>.</exception>
        /// <exception cref="ArgumentException">Thrown when <paramref name="username"/> or <paramref name="clientId"/> are blank.</exception>
        public JwtRestResponseHandler(string tokenEndpoint, string username, string password, string clientId)
            : this(new Uri(tokenEndpoint), username, password, "password", clientId)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="JwtRestResponseHandler"/> class using the specified token retrieval details.
        /// </summary>
        /// <param name="tokenEndpoint">Token endpoint.</param>
        /// <param name="username">Client username.</param>
        /// <param name="password">Client password.</param>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="tokenEndpoint"/>, <paramref name="username"/> or <paramref name="password"/> are <c>null</c>.</exception>
        /// <exception cref="ArgumentException">Thrown when <paramref name="username"/> is blank.</exception>
        public JwtRestResponseHandler(Uri tokenEndpoint, string username, string password)
            : this(tokenEndpoint, username, password, "password", null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="JwtRestResponseHandler"/> class using the specified token retrieval details.
        /// </summary>
        /// <param name="tokenEndpoint">Token endpoint.</param>
        /// <param name="username">Client username.</param>
        /// <param name="password">Client password.</param>
        /// <param name="clientId">Client id.</param>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="tokenEndpoint"/>, <paramref name="username"/>, <paramref name="password"/> or <paramref name="clientId"/> are <c>null</c>.</exception>
        /// <exception cref="ArgumentException">Thrown when <paramref name="username"/> or <paramref name="clientId"/> are blank.</exception>
        public JwtRestResponseHandler(Uri tokenEndpoint, string username, string password, string clientId)
            : this(tokenEndpoint, username, password, "password", clientId)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="JwtRestResponseHandler"/> class using the specified token retrieval details.
        /// </summary>
        /// <param name="tokenEndpoint">Token endpoint.</param>
        /// <param name="username">Client username.</param>
        /// <param name="password">Client password.</param>
        /// <param name="grantType">Grant type.</param>
        /// <param name="clientId">Client id.</param>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="tokenEndpoint"/>, <paramref name="username"/>, <paramref name="password"/> or <paramref name="grantType"/> are <c>null</c>.</exception>
        /// <exception cref="ArgumentException">Thrown when <paramref name="username"/>, <paramref name="grantType"/> or <paramref name="clientId"/> are blank.</exception>
        public JwtRestResponseHandler(Uri tokenEndpoint, string username, string password, string grantType, string clientId)
            : base()
        {
            if (username == null)
            {
                throw new ArgumentNullException(nameof(username));
            }

            if (password == null)
            {
                throw new ArgumentNullException(nameof(username));
            }

            if (grantType == null)
            {
                throw new ArgumentNullException(nameof(username));
            }

            if (string.IsNullOrWhiteSpace(username))
            {
                throw new ArgumentException("Username must not be blank.", nameof(username));
            }

            if (string.IsNullOrWhiteSpace(grantType))
            {
                throw new ArgumentException("Grant type must not be blank.", nameof(grantType));
            }

            if (clientId != null)
            {
                if (string.IsNullOrWhiteSpace(clientId))
                {
                    throw new ArgumentException("Client id must not be blank if not null.", nameof(clientId));
                }
            }

            TokenEndpoint = tokenEndpoint ?? throw new ArgumentNullException(nameof(tokenEndpoint));
            Username = username;
            Password = password;
            GrantType = grantType;
            ClientId = clientId;
            HttpClientFactoryFactory = JwtHttpClientFactoryFactory;
        }

        /// <summary>
        /// Gets or sets the token endpoint.
        /// </summary>
        /// <value>Token endpoint.</value>
        public Uri TokenEndpoint { get; set; }

        /// <summary>
        /// Gets or sets the username.
        /// </summary>
        /// <value>Username.</value>
        public string Username { get; set; }

        /// <summary>
        /// Gets or sets the password.
        /// </summary>
        /// <value>Password.</value>
        public string Password { get; set; }

        /// <summary>
        /// Gets or sets the grant type.
        /// </summary>
        /// <value>Grant type.</value>
        public string GrantType { get; set; }

        /// <summary>
        /// Gets or sets the client id.
        /// </summary>
        /// <value>Client id.</value>
        public string ClientId { get; set; }

        /// <summary>
        /// Returns a new <see cref="IHttpClientFactory"/> that can be used to create HTTP clients that can access secured resources.
        /// </summary>
        /// <param name="currentHttpClientFactory">Current client factory.</param>
        /// <returns>An <see cref="IHttpClientFactory"/> that creates <see cref="HttpClient"/>s that can access secured resources.</returns>
        public IHttpClientFactory JwtHttpClientFactoryFactory(IHttpClientFactory currentHttpClientFactory)
        {
            // construct the client and content to send to the token endpoint
            var client = RestClientFactory.Create();
            client.HttpClientFactory = new StandardHttpClientFactory() { CertificateValidation = CertificateValidation.AcceptAny };

            using (var content = new StringContent("username=" + WebUtility.UrlEncode(Username) + "&password=" + WebUtility.UrlEncode(Password) + "&grant_type=" + WebUtility.UrlEncode(GrantType) + (ClientId != null ? "&client_id=" + WebUtility.UrlEncode(ClientId) : string.Empty)))
            {
                content.Headers.ContentType = new MediaTypeHeaderValue("application/x-www-form-urlencoded");

                // obtain access token
                var result = client.PostContent(TokenEndpoint, content);
                var json = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(result);
                var accessToken = json.access_token;

                // return factory that uses the access token
                var restAuthenticationValue = new AuthenticationHeaderValue("Bearer", accessToken.Value);
                return IHttpClientFactoryFactory.Create(currentHttpClientFactory, restAuthenticationValue);
            }
        }
    }
}
