﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using SimpleSyndicate.Web.HttpClientFactories;

namespace SimpleSyndicate.Web.RestResponseHandlers
{
    /// <summary>
    /// Authenticated response handler for <see cref="RestClient"/>s using <c>OAuth2</c> services that use client credentials for access; if the service responds with
    /// <see cref="HttpStatusCode.Unauthorized"/>, it will automatically re-authenticate and re-try the request.
    /// </summary>
    public class OAuth2ClientCredentialsRestResponseHandler : AuthenticatedRestResponseHandlerBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="OAuth2ClientCredentialsRestResponseHandler"/> class using the specified token retrieval details.
        /// </summary>
        /// <param name="tokenEndpoint">Token endpoint.</param>
        /// <param name="clientId">Client id.</param>
        /// <param name="clientSecret">Client secret.</param>
        /// <param name="scope">Token scope.</param>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="tokenEndpoint"/>, <paramref name="clientId"/>, <paramref name="clientSecret"/> or <paramref name="scope"/> are <c>null</c>.</exception>
        /// <exception cref="ArgumentException">Thrown when <paramref name="clientId"/>, <paramref name="clientSecret"/> or <paramref name="scope"/> are blank.</exception>
        public OAuth2ClientCredentialsRestResponseHandler(string tokenEndpoint, string clientId, string clientSecret, string scope)
            : this(new Uri(tokenEndpoint), clientId, clientSecret, scope)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="OAuth2ClientCredentialsRestResponseHandler"/> class using the specified token retrieval details.
        /// </summary>
        /// <param name="tokenEndpoint">Token endpoint.</param>
        /// <param name="clientId">Client id.</param>
        /// <param name="clientSecret">Client secret.</param>
        /// <param name="scope">Token scope.</param>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="tokenEndpoint"/>, <paramref name="clientId"/>, <paramref name="clientSecret"/> or <paramref name="scope"/> are <c>null</c>.</exception>
        /// <exception cref="ArgumentException">Thrown when <paramref name="clientId"/>, <paramref name="clientSecret"/> or <paramref name="scope"/> are blank.</exception>
        public OAuth2ClientCredentialsRestResponseHandler(Uri tokenEndpoint, string clientId, string clientSecret, string scope)
            : base()
        {
            if (clientId == null)
            {
                throw new ArgumentNullException(nameof(clientId));
            }

            if (string.IsNullOrWhiteSpace(clientId))
            {
                throw new ArgumentException("Client id must not be blank.", nameof(clientId));
            }

            if (clientSecret == null)
            {
                throw new ArgumentNullException(nameof(clientSecret));
            }

            if (string.IsNullOrWhiteSpace(clientSecret))
            {
                throw new ArgumentException("Client secret must not be blank.", nameof(clientSecret));
            }

            TokenEndpoint = tokenEndpoint ?? throw new ArgumentNullException(nameof(tokenEndpoint));
            ClientId = clientId;
            ClientSecret = clientSecret;
            Scope = scope;
            HttpClientFactoryFactory = OAuth2HttpClientFactoryFactory;
        }

        /// <summary>
        /// Gets or sets the token endpoint.
        /// </summary>
        /// <value>Token endpoint.</value>
        public Uri TokenEndpoint { get; set; }

        /// <summary>
        /// Gets or sets the client id.
        /// </summary>
        /// <value>Client id.</value>
        public string ClientId { get; set; }

        /// <summary>
        /// Gets or sets the client secret.
        /// </summary>
        /// <value>Client secret.</value>
        public string ClientSecret { get; set; }

        /// <summary>
        /// Gets or sets the scope.
        /// </summary>
        /// <value>Authentication scope.</value>
        public string Scope { get; set; }

        /// <summary>
        /// Returns a new <see cref="IHttpClientFactory"/> that can be used to create HTTP clients that can access secured resources.
        /// </summary>
        /// <param name="currentHttpClientFactory">Current client factory.</param>
        /// <returns>An <see cref="IHttpClientFactory"/> that creates <see cref="HttpClient"/>s that can access secured resources.</returns>
        public IHttpClientFactory OAuth2HttpClientFactoryFactory(IHttpClientFactory currentHttpClientFactory)
        {
            // construct the client and content to send to the token endpoint
            var authenticationHeaderValue = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(ClientId + ":" + ClientSecret)));
            var client = RestClientFactory.Create(authenticationHeaderValue);
            using (var content = new StringContent("grant_type=client_credentials" + (string.IsNullOrWhiteSpace(Scope) ? string.Empty : "&scope=" + WebUtility.UrlEncode(Scope))))
            {
                content.Headers.ContentType = new MediaTypeHeaderValue("application/x-www-form-urlencoded");

                // obtain access token
                var result = client.PostContent(TokenEndpoint, content);
                var json = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(result);
                var accessToken = json.access_token;

                // return factory that uses the access token
                var restAuthenticationValue = new AuthenticationHeaderValue("Bearer", accessToken.Value);
                return IHttpClientFactoryFactory.Create(currentHttpClientFactory, restAuthenticationValue);
            }
        }
    }
}
