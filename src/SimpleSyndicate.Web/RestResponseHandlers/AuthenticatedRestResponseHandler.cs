﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System;
using System.Net;

namespace SimpleSyndicate.Web.RestResponseHandlers
{
    /// <summary>
    /// Authenticated response handler for <see cref="RestClient"/>s; if the service responds with <see cref="HttpStatusCode.Unauthorized"/>, it will automatically re-authenticate and re-try the request.
    /// </summary>
    public class AuthenticatedRestResponseHandler : AuthenticatedRestResponseHandlerBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AuthenticatedRestResponseHandler"/> class using the specified <paramref name="httpClientFactoryFactory"/>.
        /// </summary>
        /// <param name="httpClientFactoryFactory">An <see cref="Func{IHttpClientFactory}"/> that is used to create a HTTP client factory that creates HTTP clients used to make authenticated REST requests.</param>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="httpClientFactoryFactory"/> is <c>null</c>.</exception>
        public AuthenticatedRestResponseHandler(Func<IHttpClientFactory, IHttpClientFactory> httpClientFactoryFactory)
            : base()
        {
            HttpClientFactoryFactory = httpClientFactoryFactory ?? throw new ArgumentNullException(nameof(httpClientFactoryFactory));
        }
    }
}
