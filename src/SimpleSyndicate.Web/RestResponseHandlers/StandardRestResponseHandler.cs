﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net;
using System.Net.Http;

namespace SimpleSyndicate.Web.RestResponseHandlers
{
    /// <summary>
    /// Default response handler for <see cref="RestClient"/>s.
    /// </summary>
    public class StandardRestResponseHandler : IRestResponseHandler
    {
        /// <summary>
        /// Always returns <c>null</c> as the standard handler assumes no re-trying is needed, or possible.
        /// </summary>
        /// <param name="response">REST response to handle.</param>
        /// <param name="currentHttpClientFactory">Current client factory; if a new client factory is returned it will be based on the current one.</param>
        /// <returns>Always <c>null</c>.</returns>
        public virtual IHttpClientFactory NewHttpClientFactoryToRetryRequestWith(HttpResponseMessage response, IHttpClientFactory currentHttpClientFactory)
        {
            if (response == null)
            {
                throw new ArgumentNullException(nameof(response));
            }

            if (currentHttpClientFactory == null)
            {
                throw new ArgumentNullException(nameof(currentHttpClientFactory));
            }

            return null;
        }

        /// <summary>
        /// Handles the specified response, throwing an exception if the response is not valid.
        /// </summary>
        /// <param name="response">REST response to handle.</param>
        /// <returns>The <see cref="IRestResponseHandler"/>.</returns>
        /// <exception cref="InvalidOperationException">Thrown if the <paramref name="response"/> does not have a valid status code (i.e not <c>Ok</c>, <c>NotFound</c> or <c>Created</c>).</exception>
        public virtual IRestResponseHandler HandleResponse(HttpResponseMessage response)
        {
            if (response == null)
            {
                throw new ArgumentNullException(nameof(response));
            }

            if (AllowStatusCode(response.StatusCode))
            {
                return this;
            }

            var e = new InvalidOperationException(
                    string.Format(
                        CultureInfo.CurrentCulture,
                        "{1} ({2}) returned for {0} to {3}\nContent as follows, see Data[\"HttpResponseMessage\"] for full context:\n{4}",
                        response.RequestMessage.Method,
                        (int)response.StatusCode,
                        response.ReasonPhrase,
                        response.RequestMessage.RequestUri,
                        response.Content.ReadAsStringAsync().Result));

            e.Data["HttpResponseMessage"] = response;

            throw e;
        }

        /// <summary>
        /// Returns whether the specified status code is allowed.
        /// </summary>
        /// <param name="statusCode">Status code to check.</param>
        /// <returns><c>true</c> if the status code is <c>Ok</c>, <c>NotFound</c> or <c>Created</c>; <c>false</c> otherwise.</returns>
        protected static bool AllowStatusCode(HttpStatusCode statusCode)
        {
            return new List<HttpStatusCode>()
            {
                HttpStatusCode.OK,
                HttpStatusCode.NotFound,
                HttpStatusCode.Created,
                HttpStatusCode.Accepted
            }
            .Contains(statusCode);
        }
    }
}
