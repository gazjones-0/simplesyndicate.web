﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System;
using System.Net;
using System.Net.Http;

namespace SimpleSyndicate.Web.RestResponseHandlers
{
    /// <summary>
    /// Base class for authenticated response handlers for <see cref="RestClient"/>s; if the service responds with <see cref="HttpStatusCode.Unauthorized"/>,
    /// it will automatically re-authenticate and re-try the request.
    /// </summary>
    public abstract class AuthenticatedRestResponseHandlerBase : StandardRestResponseHandler
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AuthenticatedRestResponseHandlerBase"/> class.
        /// </summary>
        protected AuthenticatedRestResponseHandlerBase() => HttpClientFactoryFactory = (IHttpClientFactory factory) => { throw new NotImplementedException("No implementation provided in derived class."); };

        /// <summary>
        /// Gets or sets a method that will return an HTTP client factory that can be used to make authenticated REST requests; when a REST request returns unauthorized
        /// this method will be called to get a new factory to create a new HTTP client to re-try the request with.
        /// </summary>
        /// <value>An <see cref="Func{IHttpClientFactory}"/>.</value>
        public Func<IHttpClientFactory, IHttpClientFactory> HttpClientFactoryFactory { get; set; }

        /// <summary>
        /// If the response is unauthorized, returns a new HTTP client factory to re-try the request with.
        /// </summary>
        /// <param name="response">REST response to handle.</param>
        /// <param name="currentHttpClientFactory">Current client factory; if a new client factory is returned it will be based on the current one.</param>
        /// <returns>An <see cref="IHttpClientFactory"/> via the <see cref="HttpClientFactoryFactory"/> if the response was unauthorized, <c>null</c> otherwise.</returns>
        public override IHttpClientFactory NewHttpClientFactoryToRetryRequestWith(HttpResponseMessage response, IHttpClientFactory currentHttpClientFactory)
        {
            if (response == null)
            {
                throw new ArgumentNullException(nameof(response));
            }

            if (currentHttpClientFactory == null)
            {
                throw new ArgumentNullException(nameof(currentHttpClientFactory));
            }

            if (response.StatusCode == HttpStatusCode.Unauthorized)
            {
                return HttpClientFactoryFactory(currentHttpClientFactory);
            }

            return null;
        }
    }
}
