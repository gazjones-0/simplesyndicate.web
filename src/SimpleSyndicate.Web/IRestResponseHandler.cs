﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System.Net.Http;

namespace SimpleSyndicate.Web
{
    /// <summary>
    /// Interface for REST response handlers, which are used by <see cref="RestClient"/>s; see <see cref="RestResponseHandlers"/> for common implementations.
    /// </summary>
    public interface IRestResponseHandler
    {
        /// <summary>
        /// Handles responses that can be re-tried by returning a new <see cref="IHttpClientFactory"/> to perform the re-try with, or returning <c>null</c>
        /// to not re-try and let the response be handled by <see cref="HandleResponse"/>.
        /// </summary>
        /// <param name="response">REST response to handle.</param>
        /// <param name="currentHttpClientFactory">Current client factory; if a new client factory is returned it will be based on the current one.</param>
        /// <returns>A new <see cref="IHttpClientFactory"/> to use to re-try the request, or <c>null</c> to do nothing.</returns>
        IHttpClientFactory NewHttpClientFactoryToRetryRequestWith(HttpResponseMessage response, IHttpClientFactory currentHttpClientFactory);

        /// <summary>
        /// Handles the specified response; an exception should be thrown if the response is not valid. This is always called last, and should be used
        /// for general response handling.
        /// </summary>
        /// <param name="response">REST response to handle.</param>
        /// <returns>The <see cref="IRestResponseHandler"/>.</returns>
        IRestResponseHandler HandleResponse(HttpResponseMessage response);
    }
}
