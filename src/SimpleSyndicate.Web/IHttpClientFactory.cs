﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System.Net.Http;
using System.Net.Http.Headers;

namespace SimpleSyndicate.Web
{
    /// <summary>
    /// Interface for factory classes that create <see cref="HttpClient"/>s; see <see cref="HttpClientFactories"/> for common implementations.
    /// </summary>
    public interface IHttpClientFactory
    {
        /// <summary>
        /// Gets or sets the <see cref="AuthenticationHeaderValue"/> that will be set when <see cref="HttpClient"/>s are created.
        /// </summary>
        /// <value>The authentication information.</value>
        AuthenticationHeaderValue AuthenticationHeaderValue { get; set; }

        /// <summary>
        /// Creates a new <see cref="HttpClient"/> object, which is set to use default credentials when making requests.
        /// </summary>
        /// <returns>An <see cref="HttpClient"/> object.</returns>
        HttpClient Create();
    }
}
