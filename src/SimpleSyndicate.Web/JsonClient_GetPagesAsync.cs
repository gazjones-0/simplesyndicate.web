﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace SimpleSyndicate.Web
{
    /// <content>
    /// Contains async paged GET-related functionality.
    /// </content>
    public partial class JsonClient
    {
        /// <overloads>
        /// <summary>
        /// Asynchronously GETs multiple pages of JSON, de-serializing the combined response.
        /// </summary>
        /// </overloads>
        /// <summary>
        /// Asynchronously retrieves pages from the specified <paramref name="address"/>, using the <paramref name="nextPageUrl"/> to retrieve subsequent pages, de-serializing
        /// the combined set of pages into a single <typeparamref name="TPage" />.
        /// </summary>
        /// <typeparam name="TPage">The type to deserialize the pages to.</typeparam>
        /// <typeparam name="TPageItem">The type to deserialize each item on the page to.</typeparam>
        /// <param name="address">Uri to the resource.</param>
        /// <param name="nextPageUrl">The expression that returns the location of the next page.</param>
        /// <param name="items">The expression that returns the items in a page.</param>
        /// <returns>The deserialized GET's JSON response.</returns>
        public async Task<TPage> GetPagesAsync<TPage, TPageItem>(string address, Expression<Func<TPage, string>> nextPageUrl, Expression<Func<TPage, IList<TPageItem>>> items) => await GetPagesAsync<TPage, TPageItem>(new Uri(address), nextPageUrl, items).ConfigureAwait(false);

        /// <summary>
        /// Asynchronously retrieves pages from the URI formed from the <paramref name="baseAddress"/> and the <paramref name="path"/>, using the <paramref name="nextPageUrl"/> to retrieve subsequent pages, de-serializing
        /// the combined set of pages into a single <typeparamref name="TPage" />.
        /// </summary>
        /// <typeparam name="TPage">The type to deserialize the pages to.</typeparam>
        /// <typeparam name="TPageItem">The type to deserialize each item on the page to.</typeparam>
        /// <param name="baseAddress">Base address.</param>
        /// <param name="path">Path to resource.</param>
        /// <param name="nextPageUrl">The expression that returns the location of the next page.</param>
        /// <param name="items">The expression that returns the items in a page.</param>
        /// <returns>The deserialized GET's JSON response from the URI formed from the <paramref name="baseAddress"/> and the <paramref name="path"/>.</returns>
        public async Task<TPage> GetPagesAsync<TPage, TPageItem>(string baseAddress, string path, Expression<Func<TPage, string>> nextPageUrl, Expression<Func<TPage, IList<TPageItem>>> items)
        {
            var parameters = new NameValueCollection();
            return await GetPagesAsync<TPage, TPageItem>(baseAddress, path, parameters, nextPageUrl, items).ConfigureAwait(false);
        }

        /// <summary>
        /// Asynchronously retrieves pages from the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and single parameter, using the <paramref name="nextPageUrl"/> to retrieve subsequent pages, de-serializing
        /// the combined set of pages into a single <typeparamref name="TPage" />.
        /// </summary>
        /// <typeparam name="TPage">The type to deserialize the pages to.</typeparam>
        /// <typeparam name="TPageItem">The type to deserialize each item on the page to.</typeparam>
        /// <param name="baseAddress">Base address.</param>
        /// <param name="path">Path to resource.</param>
        /// <param name="parameterName">Name of the parameter.</param>
        /// <param name="parameterValue">Value of the parameter.</param>
        /// <param name="nextPageUrl">The expression that returns the location of the next page.</param>
        /// <param name="items">The expression that returns the items in a page.</param>
        /// <returns>The deserialized GET's JSON response from the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and single parameter.</returns>
        public async Task<TPage> GetPagesAsync<TPage, TPageItem>(string baseAddress, string path, string parameterName, string parameterValue, Expression<Func<TPage, string>> nextPageUrl, Expression<Func<TPage, IList<TPageItem>>> items)
        {
            var parameters = new NameValueCollection()
            {
                { parameterName, parameterValue }
            };
            return await GetPagesAsync<TPage, TPageItem>(baseAddress, path, parameters, nextPageUrl, items).ConfigureAwait(false);
        }

        /// <summary>
        /// Asynchronously retrieves pages from the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and two parameters, using the <paramref name="nextPageUrl"/> to retrieve subsequent pages, de-serializing
        /// the combined set of pages into a single <typeparamref name="TPage" />.
        /// </summary>
        /// <typeparam name="TPage">The type to deserialize the pages to.</typeparam>
        /// <typeparam name="TPageItem">The type to deserialize each item on the page to.</typeparam>
        /// <param name="baseAddress">Base address.</param>
        /// <param name="path">Path to resource.</param>
        /// <param name="parameterName1">Name of the first parameter.</param>
        /// <param name="parameterValue1">Value of the first parameter.</param>
        /// <param name="parameterName2">Name of the second parameter.</param>
        /// <param name="parameterValue2">Value of the second parameter.</param>
        /// <param name="nextPageUrl">The expression that returns the location of the next page.</param>
        /// <param name="items">The expression that returns the items in a page.</param>
        /// <returns>The deserialized GET's JSON response from the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and two parameters.</returns>
        public async Task<TPage> GetPagesAsync<TPage, TPageItem>(string baseAddress, string path, string parameterName1, string parameterValue1, string parameterName2, string parameterValue2, Expression<Func<TPage, string>> nextPageUrl, Expression<Func<TPage, IList<TPageItem>>> items)
        {
            var parameters = new NameValueCollection
            {
                { parameterName1, parameterValue1 },
                { parameterName2, parameterValue2 }
            };
            return await GetPagesAsync<TPage, TPageItem>(baseAddress, path, parameters, nextPageUrl, items).ConfigureAwait(false);
        }

        /// <summary>
        /// Asynchronously retrieves pages from the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and three parameters, using the <paramref name="nextPageUrl"/> to retrieve subsequent pages, de-serializing
        /// the combined set of pages into a single <typeparamref name="TPage" />.
        /// </summary>
        /// <typeparam name="TPage">The type to deserialize the pages to.</typeparam>
        /// <typeparam name="TPageItem">The type to deserialize each item on the page to.</typeparam>
        /// <param name="baseAddress">Base address.</param>
        /// <param name="path">Path to resource.</param>
        /// <param name="parameterName1">Name of the first parameter.</param>
        /// <param name="parameterValue1">Value of the first parameter.</param>
        /// <param name="parameterName2">Name of the second parameter.</param>
        /// <param name="parameterValue2">Value of the second parameter.</param>
        /// <param name="parameterName3">Name of the third parameter.</param>
        /// <param name="parameterValue3">Value of the third parameter.</param>
        /// <param name="nextPageUrl">The expression that returns the location of the next page.</param>
        /// <param name="items">The expression that returns the items in a page.</param>
        /// <returns>The deserialized GET's JSON response from the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and three parameters.</returns>
        public async Task<TPage> GetPagesAsync<TPage, TPageItem>(string baseAddress, string path, string parameterName1, string parameterValue1, string parameterName2, string parameterValue2, string parameterName3, string parameterValue3, Expression<Func<TPage, string>> nextPageUrl, Expression<Func<TPage, IList<TPageItem>>> items)
        {
            var parameters = new NameValueCollection
            {
                { parameterName1, parameterValue1 },
                { parameterName2, parameterValue2 },
                { parameterName3, parameterValue3 }
            };
            return await GetPagesAsync<TPage, TPageItem>(baseAddress, path, parameters, nextPageUrl, items).ConfigureAwait(false);
        }

        /// <summary>
        /// Asynchronously retrieves pages from the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and parameter collection, using the <paramref name="nextPageUrl"/> to retrieve subsequent pages, de-serializing
        /// the combined set of pages into a single <typeparamref name="TPage" />.
        /// </summary>
        /// <typeparam name="TPage">The type to deserialize the pages to.</typeparam>
        /// <typeparam name="TPageItem">The type to deserialize each item on the page to.</typeparam>
        /// <param name="baseAddress">Base address.</param>
        /// <param name="path">Path to resource.</param>
        /// <param name="parameters">Collection of parameters.</param>
        /// <param name="nextPageUrl">The expression that returns the location of the next page.</param>
        /// <param name="items">The expression that returns the items in a page.</param>
        /// <returns>The deserialized GET's JSON response from the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and parameter collection.</returns>
        public async Task<TPage> GetPagesAsync<TPage, TPageItem>(string baseAddress, string path, NameValueCollection parameters, Expression<Func<TPage, string>> nextPageUrl, Expression<Func<TPage, IList<TPageItem>>> items) => await GetPagesAsync<TPage, TPageItem>(UriHelpers.BuildUri(baseAddress, path, parameters), nextPageUrl, items).ConfigureAwait(false);

        /// <summary>
        /// Asynchronously retrieves pages from the specified <paramref name="uri"/>, using the <paramref name="nextPageUrl"/> to retrieve subsequent pages, de-serializing
        /// the combined set of pages into a single <typeparamref name="TPage" />.
        /// </summary>
        /// <typeparam name="TPage">The type to deserialize the pages to.</typeparam>
        /// <typeparam name="TPageItem">The type to deserialize each item on the page to.</typeparam>
        /// <param name="uri">Uri to the resource.</param>
        /// <param name="nextPageUrl">The expression that returns the location of the next page.</param>
        /// <param name="items">The expression that returns the items in a page.</param>
        /// <returns>The combined set of pages from the specified <paramref name="uri"/>.</returns>
        public async Task<TPage> GetPagesAsync<TPage, TPageItem>(Uri uri, Expression<Func<TPage, string>> nextPageUrl, Expression<Func<TPage, IList<TPageItem>>> items) => await PerformPagedJsonOperationAsync<TPage, TPageItem>(uri, nextPageUrl, items).ConfigureAwait(false);

        /// <summary>
        /// Asynchronously retrieves pages from the specified <paramref name="address"/>, using the <paramref name="nextPageUrl"/> to retrieve subsequent pages, de-serializing
        /// the combined set of pages into a single <typeparamref name="TPage" />; a specified <paramref name="itemAction"/> is performed on each item retrieved.
        /// </summary>
        /// <typeparam name="TPage">The type to deserialize the pages to.</typeparam>
        /// <typeparam name="TPageItem">The type to deserialize each item on the page to.</typeparam>
        /// <param name="address">Uri to the resource.</param>
        /// <param name="nextPageUrl">The expression that returns the location of the next page.</param>
        /// <param name="items">The expression that returns the items in a page.</param>
        /// <param name="itemAction">An action to perform on each item.</param>
        /// <returns>The deserialized GET's JSON response.</returns>
        public async Task<TPage> GetPagesAsync<TPage, TPageItem>(string address, Expression<Func<TPage, string>> nextPageUrl, Expression<Func<TPage, IList<TPageItem>>> items, Action<TPageItem> itemAction) => await GetPagesAsync<TPage, TPageItem>(new Uri(address), nextPageUrl, items, itemAction).ConfigureAwait(false);

        /// <summary>
        /// Asynchronously retrieves pages from the URI formed from the <paramref name="baseAddress"/> and the <paramref name="path"/>, using the <paramref name="nextPageUrl"/> to retrieve subsequent pages, de-serializing
        /// the combined set of pages into a single <typeparamref name="TPage" />; a specified <paramref name="itemAction"/> is performed on each item retrieved.
        /// </summary>
        /// <typeparam name="TPage">The type to deserialize the pages to.</typeparam>
        /// <typeparam name="TPageItem">The type to deserialize each item on the page to.</typeparam>
        /// <param name="baseAddress">Base address.</param>
        /// <param name="path">Path to resource.</param>
        /// <param name="nextPageUrl">The expression that returns the location of the next page.</param>
        /// <param name="items">The expression that returns the items in a page.</param>
        /// <param name="itemAction">An action to perform on each item.</param>
        /// <returns>The deserialized GET's JSON response from the URI formed from the <paramref name="baseAddress"/> and the <paramref name="path"/>.</returns>
        public async Task<TPage> GetPagesAsync<TPage, TPageItem>(string baseAddress, string path, Expression<Func<TPage, string>> nextPageUrl, Expression<Func<TPage, IList<TPageItem>>> items, Action<TPageItem> itemAction)
        {
            var parameters = new NameValueCollection();
            return await GetPagesAsync<TPage, TPageItem>(baseAddress, path, parameters, nextPageUrl, items, itemAction).ConfigureAwait(false);
        }

        /// <summary>
        /// Asynchronously retrieves pages from the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and single parameter, using the <paramref name="nextPageUrl"/> to retrieve subsequent pages, de-serializing
        /// the combined set of pages into a single <typeparamref name="TPage" />; a specified <paramref name="itemAction"/> is performed on each item retrieved.
        /// </summary>
        /// <typeparam name="TPage">The type to deserialize the pages to.</typeparam>
        /// <typeparam name="TPageItem">The type to deserialize each item on the page to.</typeparam>
        /// <param name="baseAddress">Base address.</param>
        /// <param name="path">Path to resource.</param>
        /// <param name="parameterName">Name of the parameter.</param>
        /// <param name="parameterValue">Value of the parameter.</param>
        /// <param name="nextPageUrl">The expression that returns the location of the next page.</param>
        /// <param name="items">The expression that returns the items in a page.</param>
        /// <param name="itemAction">An action to perform on each item.</param>
        /// <returns>The deserialized GET's JSON response from the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and single parameter.</returns>
        public async Task<TPage> GetPagesAsync<TPage, TPageItem>(string baseAddress, string path, string parameterName, string parameterValue, Expression<Func<TPage, string>> nextPageUrl, Expression<Func<TPage, IList<TPageItem>>> items, Action<TPageItem> itemAction)
        {
            var parameters = new NameValueCollection()
            {
                { parameterName, parameterValue }
            };
            return await GetPagesAsync<TPage, TPageItem>(baseAddress, path, parameters, nextPageUrl, items, itemAction).ConfigureAwait(false);
        }

        /// <summary>
        /// Asynchronously retrieves pages from the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and two parameters, using the <paramref name="nextPageUrl"/> to retrieve subsequent pages, de-serializing
        /// the combined set of pages into a single <typeparamref name="TPage" />; a specified <paramref name="itemAction"/> is performed on each item retrieved.
        /// </summary>
        /// <typeparam name="TPage">The type to deserialize the pages to.</typeparam>
        /// <typeparam name="TPageItem">The type to deserialize each item on the page to.</typeparam>
        /// <param name="baseAddress">Base address.</param>
        /// <param name="path">Path to resource.</param>
        /// <param name="parameterName1">Name of the first parameter.</param>
        /// <param name="parameterValue1">Value of the first parameter.</param>
        /// <param name="parameterName2">Name of the second parameter.</param>
        /// <param name="parameterValue2">Value of the second parameter.</param>
        /// <param name="nextPageUrl">The expression that returns the location of the next page.</param>
        /// <param name="items">The expression that returns the items in a page.</param>
        /// <param name="itemAction">An action to perform on each item.</param>
        /// <returns>The deserialized GET's JSON response from the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and two parameters.</returns>
        public async Task<TPage> GetPagesAsync<TPage, TPageItem>(string baseAddress, string path, string parameterName1, string parameterValue1, string parameterName2, string parameterValue2, Expression<Func<TPage, string>> nextPageUrl, Expression<Func<TPage, IList<TPageItem>>> items, Action<TPageItem> itemAction)
        {
            var parameters = new NameValueCollection
            {
                { parameterName1, parameterValue1 },
                { parameterName2, parameterValue2 }
            };
            return await GetPagesAsync<TPage, TPageItem>(baseAddress, path, parameters, nextPageUrl, items, itemAction).ConfigureAwait(false);
        }

        /// <summary>
        /// Asynchronously retrieves pages from the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and three parameters, using the <paramref name="nextPageUrl"/> to retrieve subsequent pages, de-serializing
        /// the combined set of pages into a single <typeparamref name="TPage" />; a specified <paramref name="itemAction"/> is performed on each item retrieved.
        /// </summary>
        /// <typeparam name="TPage">The type to deserialize the pages to.</typeparam>
        /// <typeparam name="TPageItem">The type to deserialize each item on the page to.</typeparam>
        /// <param name="baseAddress">Base address.</param>
        /// <param name="path">Path to resource.</param>
        /// <param name="parameterName1">Name of the first parameter.</param>
        /// <param name="parameterValue1">Value of the first parameter.</param>
        /// <param name="parameterName2">Name of the second parameter.</param>
        /// <param name="parameterValue2">Value of the second parameter.</param>
        /// <param name="parameterName3">Name of the third parameter.</param>
        /// <param name="parameterValue3">Value of the third parameter.</param>
        /// <param name="nextPageUrl">The expression that returns the location of the next page.</param>
        /// <param name="items">The expression that returns the items in a page.</param>
        /// <param name="itemAction">An action to perform on each item.</param>
        /// <returns>The deserialized GET's JSON response from the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and three parameters.</returns>
        public async Task<TPage> GetPagesAsync<TPage, TPageItem>(string baseAddress, string path, string parameterName1, string parameterValue1, string parameterName2, string parameterValue2, string parameterName3, string parameterValue3, Expression<Func<TPage, string>> nextPageUrl, Expression<Func<TPage, IList<TPageItem>>> items, Action<TPageItem> itemAction)
        {
            var parameters = new NameValueCollection
            {
                { parameterName1, parameterValue1 },
                { parameterName2, parameterValue2 },
                { parameterName3, parameterValue3 }
            };
            return await GetPagesAsync<TPage, TPageItem>(baseAddress, path, parameters, nextPageUrl, items, itemAction).ConfigureAwait(false);
        }

        /// <summary>
        /// Asynchronously retrieves pages from the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and parameter collection, using the <paramref name="nextPageUrl"/> to retrieve subsequent pages, de-serializing
        /// the combined set of pages into a single <typeparamref name="TPage" />; a specified <paramref name="itemAction"/> is performed on each item retrieved.
        /// </summary>
        /// <typeparam name="TPage">The type to deserialize the pages to.</typeparam>
        /// <typeparam name="TPageItem">The type to deserialize each item on the page to.</typeparam>
        /// <param name="baseAddress">Base address.</param>
        /// <param name="path">Path to resource.</param>
        /// <param name="parameters">Collection of parameters.</param>
        /// <param name="nextPageUrl">The expression that returns the location of the next page.</param>
        /// <param name="items">The expression that returns the items in a page.</param>
        /// <param name="itemAction">An action to perform on each item.</param>
        /// <returns>The deserialized GET's JSON response from the URI formed from the <paramref name="baseAddress"/>, <paramref name="path"/> and parameter collection.</returns>
        public async Task<TPage> GetPagesAsync<TPage, TPageItem>(string baseAddress, string path, NameValueCollection parameters, Expression<Func<TPage, string>> nextPageUrl, Expression<Func<TPage, IList<TPageItem>>> items, Action<TPageItem> itemAction) => await GetPagesAsync<TPage, TPageItem>(UriHelpers.BuildUri(baseAddress, path, parameters), nextPageUrl, items, itemAction).ConfigureAwait(false);

        /// <summary>
        /// Asynchronously retrieves pages from the specified <paramref name="uri"/>, using the <paramref name="nextPageUrl"/> to retrieve subsequent pages, de-serializing
        /// the combined set of pages into a single <typeparamref name="TPage" />; a specified <paramref name="itemAction"/> is performed on each item retrieved.
        /// </summary>
        /// <typeparam name="TPage">The type to deserialize the pages to.</typeparam>
        /// <typeparam name="TPageItem">The type to deserialize each item on the page to.</typeparam>
        /// <param name="uri">Uri to the resource.</param>
        /// <param name="nextPageUrl">The expression that returns the location of the next page.</param>
        /// <param name="items">The expression that returns the items in a page.</param>
        /// <param name="itemAction">An action to perform on each item.</param>
        /// <returns>The combined set of pages from the specified <paramref name="uri"/>.</returns>
        public async Task<TPage> GetPagesAsync<TPage, TPageItem>(Uri uri, Expression<Func<TPage, string>> nextPageUrl, Expression<Func<TPage, IList<TPageItem>>> items, Action<TPageItem> itemAction) => await PerformPagedJsonOperationAsync<TPage, TPageItem>(uri, nextPageUrl, items, itemAction).ConfigureAwait(false);
    }
}
