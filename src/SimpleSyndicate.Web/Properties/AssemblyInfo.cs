﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System;
using System.Reflection;
using System.Runtime.InteropServices;

// general information
[assembly: AssemblyTitle("SimpleSyndicate.Web")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("simpleSyndicate")]
[assembly: AssemblyProduct("SimpleSyndicate.Web")]
[assembly: AssemblyCopyright("Copyright © simpleSyndicate 2016, 2017, 2018")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// version
[assembly: AssemblyVersion("3.0.1")]
[assembly: AssemblyFileVersion("3.0.1")]

// CLS compliance
[assembly: CLSCompliant(true)]

// COM visibility
[assembly: ComVisible(false)]
