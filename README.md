# README #

SimpleSyndicate.Web NuGet package.

Get the package from https://www.nuget.org/packages/SimpleSyndicate.Web

### What is this repository for? ###

* Common web-related functionality, including JSON and REST clients that simplify interacting with services.

### How do I get started? ###

* See the documentation at http://gazooka_g72.bitbucket.org/SimpleSyndicate

### Reporting issues ###

* Use the tracker at https://bitbucket.org/gazooka_g72/simplesyndicate.web/issues?status=new&status=open
